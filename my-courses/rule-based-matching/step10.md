<br/>

The [to_disk](https://spacy.io/api/entityruler#to_disk) and [from_disk](https://spacy.io/api/entityruler#from_disk) let you save and load patterns to and from JSONL (newline-delimited JSON) files, containing one pattern object per line.

```
{"label": "ORG", "pattern": "Apple"} 
{"label": "GPE", "pattern": [{"LOWER": "san"}, {"LOWER": "francisco"}]}
```

```
ruler.to_disk("./patterns.jsonl")
new_ruler = EntityRuler(nlp).from_disk("./patterns.jsonl")
```

> #### Integration with Prodigy
> If you’re using the [Prodigy](https://prodi.gy/) annotation tool, you might recognize these pattern files from bootstrapping your named entity and text classification labelling. The patterns for the `EntityRuler` follow the same syntax, so you can use your existing Prodigy pattern files in spaCy, and vice versa.

When you save out an `nlp` object that has an `EntityRuler` added to its pipeline, its patterns are automatically exported to the model directory:

```
nlp = spacy.load("en_core_web_sm")
ruler = EntityRuler(nlp)
ruler.add_patterns([{"label": "ORG", "pattern": "Apple"}])
nlp.add_pipe(ruler)
nlp.to_disk("/path/to/model")
```

The saved model now includes the `"entity_ruler"` in its `"pipeline"` setting in the `meta.json`, and the model directory contains a file `entityruler.jsonl` with the patterns. When you load the model back in, all pipeline components will be restored and deserialized – including the entity ruler. This lets you ship powerful model packages with binary weights and rules included!

## Using a large number of phrase patterns 

When using a large amount of **phrase patterns** (roughly > 10000) it’s useful to understand how the `add_patterns` function of the EntityRuler works. For each **phrase pattern**, the EntityRuler calls the nlp object to construct a doc object. This happens in case you try to add the EntityRuler at the end of an existing pipeline with, for example, a POS tagger and want to extract matches based on the pattern’s POS signature.

<br/>

In this case you would pass a config value of `phrase_matcher_attr="POS"` for the `EntityRuler`.

<br/>

Running the full language pipeline across every pattern in a large list scales linearly and can therefore take a long time on large amounts of phrase patterns.

<br/>

As of spaCy 2.2.4 the `add_patterns` function has been refactored to use nlp.pipe on all phrase patterns resulting in about a 10x-20x speed up with 5,000-100,000 phrase patterns respectively.

<br/>

Even with this speedup (but especially if you’re using an older version) the `add_patterns` function can still take a long time.

<br/>

An easy workaround to make this function run faster is disabling the other language pipes while adding the phrase patterns.

```
entityruler = EntityRuler(nlp)
patterns = [{"label": "TEST", "pattern": str(i)} for i in range(100000)]

other_pipes = [p for p in nlp.pipe_names if p != "tagger"]
with nlp.disable_pipes(*other_pipes):
    entityruler.add_patterns(patterns)
```












