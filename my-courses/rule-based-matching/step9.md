<br/>

The [EntityRuler](https://spacy.io/api/entityruler) is an exciting new component that lets you add named entities based on pattern dictionaries, and makes it easy to combine rule-based and statistical named entity recognition for even more powerful models.

## Entity Patterns

Entity patterns are dictionaries with two keys: `"label"`, specifying the label to assign to the entity if the pattern is matched, and `"pattern"`, the match pattern. The entity ruler accepts two types of patterns:

1. **Phrase patterns** for exact string matches (string).

```{"label": "ORG", "pattern": "Apple"}```

2. **Token patterns** with one dictionary describing one token (list).

```{"label": "GPE", "pattern": [{"LOWER": "san"}, {"LOWER": "francisco"}]}```

## Using the entity ruler

The [EntityRuler](https://spacy.io/api/entityruler) is a pipeline component that’s typically added via [nlp.add_pipe](https://spacy.io/api/language#add_pipe). When the `nlp` object is called on a text, it will find matches in the doc and add them as entities to the `doc.ents`, using the specified pattern label as the entity label. If any matches were to overlap, the pattern matching most tokens takes priority. If they also happen to be equally long, then the match occuring first in the Doc is chosen.

```
from spacy.lang.en import English
from spacy.pipeline import EntityRuler

nlp = English()
ruler = EntityRuler(nlp)
patterns = [{"label": "ORG", "pattern": "Apple"},
            {"label": "GPE", "pattern": [{"LOWER": "san"}, {"LOWER": "francisco"}]}]
ruler.add_patterns(patterns)
nlp.add_pipe(ruler)

doc = nlp("Apple is opening its first big office in San Francisco.")
print([(ent.text, ent.label_) for ent in doc.ents])
```

The entity ruler is designed to integrate with spaCy’s existing statistical models and enhance the named entity recognizer. If it’s added **before the `"ner"` component**, the entity recognizer will respect the existing entity spans and adjust its predictions around it. This can significantly improve accuracy in some cases. If it’s added **after the `"ner`" component**, the entity ruler will only add spans to the `doc.ents` if they don’t overlap with existing entities predicted by the model. To overwrite overlapping entities, you can set `overwrite_ents=True` on initialization.

```
from spacy.pipeline import EntityRuler
​
nlp = spacy.load("en_core_web_sm")
ruler = EntityRuler(nlp)
patterns = [{"label": "ORG", "pattern": "MyCorp Inc."}]
ruler.add_patterns(patterns)
nlp.add_pipe(ruler)
​
doc = nlp("MyCorp Inc. is a company in the U.S.")
print([(ent.text, ent.label_) for ent in doc.ents])
```

## Validating and debugging EntityRuler patterns 

The `EntityRuler` can validate patterns against a JSON schema with the option `validate=True`. See details under [Validating and debugging patterns](https://spacy.io/usage/rule-based-matching#pattern-validation).

```ruler = EntityRuler(nlp, validate=True)```

## Adding IDs to patterns 

The [EntityRuler](https://spacy.io/api/entityruler) can also accept an `id` attribute for each pattern. Using the id attribute allows multiple patterns to be associated with the same entity.

```
from spacy.lang.en import English
from spacy.pipeline import EntityRuler

nlp = English()
ruler = EntityRuler(nlp)
patterns = [{"label": "ORG", "pattern": "Apple", "id": "apple"},
            {"label": "GPE", "pattern": [{"LOWER": "san"}, {"LOWER": "francisco"}], "id": "san-francisco"},
            {"label": "GPE", "pattern": [{"LOWER": "san"}, {"LOWER": "fran"}], "id": "san-francisco"}]
ruler.add_patterns(patterns)
nlp.add_pipe(ruler)

doc1 = nlp("Apple is opening its first big office in San Francisco.")
print([(ent.text, ent.label_, ent.ent_id_) for ent in doc1.ents])

doc2 = nlp("Apple is opening its first big office in San Fran.")
print([(ent.text, ent.label_, ent.ent_id_) for ent in doc2.ents])
```

If the `id` attribute is included in the [EntityRuler](https://spacy.io/api/entityruler) patterns, the `ent_id_` property of the matched entity is set to the id given in the patterns. So in the example above it’s easy to identify that “San Francisco” and “San Fran” are both the same entity.




