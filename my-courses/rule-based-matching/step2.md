<br/>

spaCy features a rule-matching engine, the [Matcher](https://spacy.io/api/matcher), that operates over tokens, similar to regular expressions. The rules can refer to token annotations (e.g. the token `text` or `tag_`, and flags (e.g. `IS_PUNCT`). The rule matcher also lets you pass in a custom callback to act on matches – for example, to merge entities and apply custom labels. You can also associate patterns with entity IDs, to allow some basic entity linking or disambiguation. To match large terminology lists, you can use the [PhraseMatcher](https://spacy.io/api/phrasematcher), which accepts `Doc` objects as match patterns.

## Adding patterns

Let’s say we want to enable spaCy to find a combination of three tokens:

<br/>

1. A token whose **lowercase form matches “hello”**, e.g. “Hello” or “HELLO”.
2. A token whose `is_punct` **flag is set to** `True`, i.e. any punctuation.
3. A token whose **lowercase form matches “world”**, e.g. “World” or “WORLD”.

```
[{"LOWER": "hello"}, {"IS_PUNCT": True}, {"LOWER": "world"}]
```

> #### Important note
> When writing patterns, keep in mind that **each dictionary** represents **one token**. If spaCy’s tokenization doesn’t match the tokens defined in a pattern, the pattern is not going to produce any results. When developing complex patterns, make sure to check examples against spaCy’s tokenization:
> ```
> doc = nlp("A complex-example,!")
> print([token.text for token in doc])
> ```

First, we initialize the `Matcher` with a vocab. The matcher must always share the same vocab with the documents it will operate on. We can now call [matcher.add()](https://spacy.io/api/matcher#add) with an ID and our custom pattern. The second argument lets you pass in an optional callback function to invoke on a successful match. For now, we set it to `None`.

```
import spacy
from spacy.matcher import Matcher
​
nlp = spacy.load("en_core_web_sm")
matcher = Matcher(nlp.vocab)
# Add match ID "HelloWorld" with no callback and one pattern
pattern = [{"LOWER": "hello"}, {"IS_PUNCT": True}, {"LOWER": "world"}]
matcher.add("HelloWorld", None, pattern)
​
doc = nlp("Hello, world! Hello world!")
matches = matcher(doc)
for match_id, start, end in matches:
    string_id = nlp.vocab.strings[match_id]  # Get string representation
    span = doc[start:end]  # The matched span
    print(match_id, string_id, start, end, span.text)

```

The matcher returns a list of `(match_id, start, end)` tuples – in this case, `[('15578876784678163569', 0, 3)]`, which maps to the span `doc[0:3]` of our original document. The `match_id` is the [hash value](https://spacy.io/usage/spacy-101#vocab) of the string ID “HelloWorld”. To get the string value, you can look up the ID in the [StringStore](https://spacy.io/api/stringstore).

```
for match_id, start, end in matches:
    string_id = nlp.vocab.strings[match_id]  # 'HelloWorld'
    span = doc[start:end]                    # The matched span
```

Optionally, we could also choose to add more than one pattern, for example to also match sequences without punctuation between “hello” and “world”:

```
matcher.add(
    "HelloWorld", 
    None, 
    [
        {"LOWER": "hello"}, 
        {"IS_PUNCT": True}, 
        {"LOWER": "world"}
    ], 
    [
        {"LOWER": "hello"}, 
        {"LOWER": "world"}
    ]
)   
```

By default, the matcher will only return the matches and **not do anything else**, like merge entities or assign labels. This is all up to you and can be defined individually for each pattern, by passing in a callback function as the `on_match` argument on `add()`. This is useful, because it lets you write entirely custom and **pattern-specific logic**. For example, you might want to merge some patterns into one token, while adding entity labels for other pattern types. You shouldn’t have to create different matchers for each of those processes.