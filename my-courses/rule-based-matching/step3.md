<br/>

The available token pattern keys correspond to a number of [Token attributes](https://spacy.io/api/token#attributes). The supported attributes for rule-based matching are:

| ATTRIBUTE | TYPE | DESCRIPTION |
:----------:|:----:|:-----------:|
ORTH       |  unicode  | The exact verbatim text of a token.
TEXT        |   unicode | The exact verbatim text of a token.
LOWER        | unicode   | The lowercase form of the token text.
LENGTH        | int   | The length of the token text.
IS_ALPHA, IS_ASCII, IS_DIGIT   |  bool | Token text consists of alphabetic characters, ASCII characters, digits.
IS_LOWER, IS_UPPER, IS_TITLE    | bool | Token text is in lowercase, uppercase, titlecase.
IS_PUNCT, IS_SPACE, IS_STOP      |bool | Token is punctuation, whitespace, stop word.
LIKE_NUM, LIKE_URL, LIKE_EMAIL  | bool | Token text resembles a number, URL, email.
POS, TAG, DEP, LEMMA, SHAPE      |   unicode | The token’s simple and extended part-of-speech tag, dependency label, lemma, shape.
ENT_TYPE   |	unicode | The token’s entity label.
_          | dict | Properties in [custom extension attributes](https://spacy.io/usage/processing-pipelines#custom-components-attributes).

## Extended pattern syntax and attributes

Instead of mapping to a single value, token patterns can also map to a **dictionary of properties**. For example, to specify that the value of a lemma should be part of a list of values, or to set a minimum character length. The following rich comparison attributes are available:

| ATTRIBUTE | TYPE | DESCRIPTION |
:----------:|:----:|:-----------:|
IN       |  any  | Attribute value is member of a list.
NOT_IN        |   any | Attribute value is not member of a list.
==, >=, <=, >, <        | int, float   | Attribute value is equal, greater or equal, smaller or equal, greater or smaller.

## Regular expressions 

In some cases, only matching tokens and token attributes isn’t enough – for example, you might want to match different spellings of a word, without having to add a new pattern for each spelling.

```
pattern = [
    {"TEXT": {"REGEX": "^[Uu](\.?|nited)$"}},
    {"TEXT": {"REGEX": "^[Ss](\.?|tates)$"}},
    {"LOWER": "president"}
]
```

The `REGEX` operator allows defining rules for any attribute string value, including custom attributes. It always needs to be applied to an attribute like `TEXT`, `LOWER` or `TAG`:

```
# Match different spellings of token texts
pattern = [{"TEXT": {"REGEX": "deff?in[ia]tely"}}]

# Match tokens with fine-grained POS tags starting with 'V'
pattern = [{"TAG": {"REGEX": "^V"}}]

# Match custom attribute values with regular expressions
pattern = [{"_": {"country": {"REGEX": "^[Uu](nited|\.?) ?[Ss](tates|\.?)$"}}}]
```

> #### Important note
> When using the `REGEX` operator, keep in mind that it operates on **single tokens**, not the whole text. Each expression you provide will be matched on a token. If you need to match on the whole text instead, see the details on [regex matching on the whole text](https://spacy.io/usage/rule-based-matching#regex-text).

## Matching regular expressions on the full text

If your expressions apply to multiple tokens, a simple solution is to match on the `doc.text` with `re.finditer` and use the [Doc.char_span](https://spacy.io/api/doc#char_span) method to create a `Span` from the character indices of the match. If the matched characters don’t map to one or more valid tokens, `Doc.char_span` returns `None`.

```
import re

doc = nlp("The United States of America (USA) are commonly known as the United States (U.S. or US) or America.")

expression = r"[Uu](nited|\.?) ?[Ss](tates|\.?)"
for match in re.finditer(expression, doc.text):
    start, end = match.span()
    span = doc.char_span(start, end)
    # This is a Span object or None if match doesn't map to valid token sequence
    if span is not None:
        print("Found match:", span.text)
```