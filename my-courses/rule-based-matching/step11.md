<br/>

You can combine statistical and rule-based components in a variety of ways. Rule-based components can be used to improve the accuracy of statistical models, by presetting tags, entities or sentence boundaries for specific tokens. The statistical models will usually respect these preset annotations, which sometimes improves the accuracy of other decisions. You can also use rule-based components after a statistical model to correct common errors. Finally, rule-based components can reference the attributes set by statistical models, in order to implement more abstract logic.

## Example: Expanding named entities

When using the a pretrained [named entity recognition](https://spacy.io/usage/linguistic-features/#named-entities) model to extract information from your texts, you may find that the predicted span only includes parts of the entity you’re looking for. Sometimes, this happens if statistical model predicts entities incorrectly. Other times, it happens if the way the entity type way defined in the original training corpus doesn’t match what you need for your application.

<br/>

For example, the corpus spaCy’s [English models](https://spacy.io/models/en) were trained on defines a PERSON entity as just the **person name**, without titles like “Mr.” or “Dr.”. This makes sense, because it makes it easier to resolve the entity type back to a knowledge base. But what if your application needs the full names, including the titles?

```
import spacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("Dr. Alex Smith chaired first board meeting of Acme Corp Inc.")
print([(ent.text, ent.label_) for ent in doc.ents])
```

While you could try and teach the model a new definition of the `PERSON` entity by [updating it](https://spacy.io/usage/training#example-train-ner) with more examples of spans that include the title, this might not be the most efficient approach. The existing model was trained on over 2 million words, so in order to completely change the definition of an entity type, you might need a lot of training examples. However, if you already have the predicted `PERSON` entities, you can use a rule-based approach that checks whether they come with a title and if so, expands the entity span by one token. After all, what all titles in this example have in common is that if they occur, they occur in the `previous token` right before the person entity.

```
from spacy.tokens import Span

def expand_person_entities(doc):
    new_ents = []
    for ent in doc.ents:
        # Only check for title if it's a person and not the first token
        if ent.label_ == "PERSON" and ent.start != 0:
            prev_token = doc[ent.start - 1]
            if prev_token.text in ("Dr", "Dr.", "Mr", "Mr.", "Ms", "Ms."):
                new_ent = Span(doc, ent.start - 1, ent.end, label=ent.label)
                new_ents.append(new_ent)
            else:
                new_ents.append(ent)
        else:
            new_ents.append(ent)
    doc.ents = new_ents
    return doc
```

The above function takes a `Doc` object, modifies its `doc.ents` and returns it. This is exactly what a [pipeline component](https://spacy.io/usage/processing-pipelines) does, so in order to let it run automatically when processing a text with the nlp object, we can use [nlp.add_pipe](https://spacy.io/api/language#add_pipe) to add it to the current pipeline.

```
from spacy.tokens import Span

def expand_person_entities(doc):
    new_ents = []
    for ent in doc.ents:
        if ent.label_ == "PERSON" and ent.start != 0:
            prev_token = doc[ent.start - 1]
            if prev_token.text in ("Dr", "Dr.", "Mr", "Mr.", "Ms", "Ms."):
                new_ent = Span(doc, ent.start - 1, ent.end, label=ent.label)
                new_ents.append(new_ent)
        else:
            new_ents.append(ent)
    doc.ents = new_ents
    return doc

# Add the component after the named entity recognizer
nlp.add_pipe(expand_person_entities, after='ner')

doc = nlp("Dr. Alex Smith chaired first board meeting of Acme Corp Inc.")
print([(ent.text, ent.label_) for ent in doc.ents])
```

An alternative approach would be to an [extension attribute](https://spacy.io/usage/processing-pipelines#custom-components-attributes) like `._.person_title` and add it to `Span` objects (which includes entity spans in doc.ents). The advantage here is that the entity text stays intact and can still be used to look up the name in a knowledge base. The following function takes a Span object, checks the previous token if it’s a `PERSON` entity and returns the title if one is found. The `Span.doc` attribute gives us easy access to the span’s parent document.

```
def get_person_title(span):
    if span.label_ == "PERSON" and span.start != 0:
        prev_token = span.doc[span.start - 1]
        if prev_token.text in ("Dr", "Dr.", "Mr", "Mr.", "Ms", "Ms."):
            return prev_token.text
```

We can now use the [Span.set_extension](https://spacy.io/api/span#set_extension) method to add the custom extension attribute `"person_title"`, using `get_person_title` as the getter function.

```
from spacy.tokens import Span

def get_person_title(span):
    if span.label_ == "PERSON" and span.start != 0:
        prev_token = span.doc[span.start - 1]
        if prev_token.text in ("Dr", "Dr.", "Mr", "Mr.", "Ms", "Ms."):
            return prev_token.text

# Register the Span extension as 'person_title'
Span.set_extension("person_title", getter=get_person_title)

doc = nlp("Dr Alex Smith chaired first board meeting of Acme Corp Inc.")
print([(ent.text, ent.label_, ent._.person_title) for ent in doc.ents])

```



