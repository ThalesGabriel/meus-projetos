<br/>

The matcher also lets you use quantifiers, specified as the `'OP'` key. Quantifiers let you define sequences of tokens to be matched, e.g. one or more punctuation marks, or specify optional tokens. Note that there are no nested or scoped quantifiers – instead, you can build those behaviors with `on_match` callbacks.

| OP | DESCRIPTION |
|:--:|:-----------:|
!    | Negate the pattern, by requiring it to match exactly 0 times.
?    | Make the pattern optional, by allowing it to match 0 or 1 times.
+    | Require the pattern to match 1 or more times.
*    | Allow the pattern to match zero or more times.

> Note on operator behaviour
> In versions before v2.1.0, the semantics of the `+` and `*` operators behave inconsistently. They were usually interpreted “greedily”, i.e. longer matches are returned where possible. However, if you specify two `+` and `*` patterns in a row and their matches overlap, the first operator will behave non-greedily. This quirk in the semantics is corrected in spaCy v2.1.0.

## Using wildcard token patterns 

While the token attributes offer many options to write highly specific patterns, you can also use an empty dictionary, `{}` as a wildcard representing **any token**. This is useful if you know the context of what you’re trying to match, but very little about the specific token and its characters. For example, let’s say you’re trying to extract people’s user names from your data. All you know is that they are listed as “User name: {username}“. The name itself may contain any character, but no whitespace – so you’ll know it will be handled as one token.

```
[{"ORTH": "User"}, {"ORTH": "name"}, {"ORTH": ":"}, {}]
```

## Validating and debugging patterns 

The `Matcher` can validate patterns against a JSON schema with the option `validate=True`. This is useful for debugging patterns during development, in particular for catching unsupported attributes.

```
from spacy.matcher import Matcher

matcher = Matcher(nlp.vocab, validate=True)
# Add match ID "HelloWorld" with unsupported attribute CASEINSENSITIVE
pattern = [{"LOWER": "hello"}, {"IS_PUNCT": True}, {"CASEINSENSITIVE": "world"}]
matcher.add("HelloWorld", None, pattern)
# 🚨 Raises an error:
# MatchPatternError: Invalid token patterns for matcher rule 'HelloWorld'
# Pattern 0:
# - Additional properties are not allowed ('CASEINSENSITIVE' was unexpected) [2]
```

## Adding on_match rules

To move on to a more realistic example, let’s say you’re working with a large corpus of blog articles, and you want to match all mentions of “Google I/O” (which spaCy tokenizes as `['Google', 'I', '/', 'O']`). To be safe, you only match on the uppercase versions, in case someone has written it as “Google i/o”.

```
from spacy.lang.en import English
from spacy.matcher import Matcher
from spacy.tokens import Span

nlp = English()
matcher = Matcher(nlp.vocab)

def add_event_ent(matcher, doc, i, matches):
    # Get the current match and create tuple of entity label, start and end.
    # Append entity to the doc's entity. (Don't overwrite doc.ents!)
    match_id, start, end = matches[i]
    entity = Span(doc, start, end, label="EVENT")
    doc.ents += (entity,)
    print(entity.text)

pattern = [{"ORTH": "Google"}, {"ORTH": "I"}, {"ORTH": "/"}, {"ORTH": "O"}]
matcher.add("GoogleIO", add_event_ent, pattern)
doc = nlp("This is a text about Google I/O")
matches = matcher(doc)
```

A very similar logic has been implemented in the built-in [EntityRuler](https://spacy.io/api/entityruler) by the way. It also takes care of handling overlapping matches, which you would otherwise have to take care of yourself.

<br/>

We can now call the matcher on our documents. The patterns will be matched in the order they occur in the text. The matcher will then iterate over the matches, look up the callback for the match ID that was matched, and invoke it.

```
doc = nlp(YOUR_TEXT_HERE)
matcher(doc)
```

When the callback is invoked, it is passed four arguments: the matcher itself, the document, the position of the current match, and the total list of matches. This allows you to write callbacks that consider the entire set of matched phrases, so that you can resolve overlaps and other conflicts in whatever way you prefer.

| ARGUMENT | TYPE | DESCRIPTION |
|:--------:|:----:|:-----------:|
matcher    | Matcher | The matcher instance.
doc    | Doc | The document the matcher was used on.
i    | int  | Index of the current match (matches[i]).
matches    | list  | A list of (match_id, start, end) tuples, describing the matches. A match tuple describes a span doc[start:end].




