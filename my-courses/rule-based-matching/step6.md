<br/>

Phone numbers can have many different formats and matching them is often tricky. During tokenization, spaCy will leave sequences of numbers intact and only split on whitespace and punctuation. This means that your match pattern will have to look out for number sequences of a certain length, surrounded by specific punctuation – depending on the [national conventions](https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers).

<br/>

The `IS_DIGIT` flag is not very helpful here, because it doesn’t tell us anything about the length. However, you can use the `SHAPE` flag, with each d representing a digit (up to 4 digits / characters):

```
[{"ORTH": "("}, {"SHAPE": "ddd"}, {"ORTH": ")"}, {"SHAPE": "dddd"}, {"ORTH": "-", "OP": "?"}, {"SHAPE": "dddd"}]
```

This will match phone numbers of the format **(123) 4567 8901** or **(123) 4567-8901**. To also match formats like **(123) 456 789**, you can add a second pattern using `'ddd'` in place of `'dddd'`. By hard-coding some values, you can match only certain, country-specific numbers. For example, here’s a pattern to match the most common formats of [international German numbers](https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers#Germany):

```
[{"ORTH": "+"}, {"ORTH": "49"}, {"ORTH": "(", "OP": "?"}, {"SHAPE": "dddd"},{"ORTH": ")", "OP": "?"}, {"SHAPE": "dddd", "LENGTH": 6}]
```

Depending on the formats your application needs to match, creating an extensive set of rules like this is often better than training a model. It’ll produce more predictable results, is much easier to modify and extend, and doesn’t require any training data – only a set of test cases.

```
import spacy
from spacy.matcher import Matcher

nlp = spacy.load("en_core_web_sm")
matcher = Matcher(nlp.vocab)
pattern = [{"ORTH": "("}, {"SHAPE": "ddd"}, {"ORTH": ")"}, {"SHAPE": "ddd"},
           {"ORTH": "-", "OP": "?"}, {"SHAPE": "ddd"}]
matcher.add("PHONE_NUMBER", None, pattern)

doc = nlp("Call me at (123) 456 789 or (123) 456 789!")
print([t.text for t in doc])
matches = matcher(doc)
for match_id, start, end in matches:
    span = doc[start:end]
    print(span.text)
```



