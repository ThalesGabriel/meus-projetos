<br/>

Let’s say you want to parse professional biographies and extract the person names and company names, and whether it’s a company they’re currently working at, or a previous company. One approach could be to try and train a named entity recognizer to predict `CURRENT_ORG` and `PREVIOUS_ORG` – but this distinction is very subtle and something the entity recognizer may struggle to learn. Nothing about “Acme Corp Inc.” is inherently “current” or “previous”.

<br/>

However, the syntax of the sentence holds some very important clues: we can check for trigger words like “work”, whether they’re **past tense** or **present tense**, whether company names are attached to it and whether the person is the subject. All of this information is available in the part-of-speech tags and the dependency parse.

```
doc = nlp("Alex Smith worked at Acme Corp Inc.")
print([(ent.text, ent.label_) for ent in doc.ents])
```

In this example, “worked” is the root of the sentence and is a past tense verb. Its subject is “Alex Smith”, the person who worked. “at Acme Corp Inc.” is a prepositional phrase attached to the verb “worked”. To extract this relationship, we can start by looking at the predicted `PERSON` entities, find their heads and check whether they’re attached to a trigger word like “work”. Next, we can check for prepositional phrases attached to the head and whether they contain an `ORG` entity. Finally, to determine whether the company affiliation is current, we can check the head’s part-of-speech tag.

```
person_entities = [ent for ent in doc.ents if ent.label_ == "PERSON"]
for ent in person_entities:
    # Because the entity is a spans, we need to use its root token. The head
    # is the syntactic governor of the person, e.g. the verb
    head = ent.root.head
    if head.lemma_ == "work":
        # Check if the children contain a preposition
        preps = [token for token in head.children if token.dep_ == "prep"]
        for prep in preps:
            # Check if tokens part of ORG entities are in the preposition's
            # children, e.g. at -> Acme Corp Inc.
            orgs = [token for token in prep.children if token.ent_type_ == "ORG"]
            # If the verb is in past tense, the company was a previous company
            print({'person': ent, 'orgs': orgs, 'past': head.tag_ == "VBD"})
```

To apply this logic automatically when we process a text, we can add it to the `nlp` object as a [custom pipeline component](https://spacy.io/usage/processing-pipelines#custom-components). The above logic also expects that entities are merged into single tokens. spaCy ships with a handy built-in `merge_entities` that takes care of that. Instead of just printing the result, you could also write it to [custom attributes](https://spacy.io/usage/processing-pipelines#custom-components-attributes) on the entity `Span` – for example `._.orgs` or `._.prev_orgs` and `._.current_orgs`.

```
from spacy.pipeline import merge_entities
from spacy import displacy

def extract_person_orgs(doc):
    person_entities = [ent for ent in doc.ents if ent.label_ == "PERSON"]
    for ent in person_entities:
        head = ent.root.head
        if head.lemma_ == "work":
            preps = [token for token in head.children if token.dep_ == "prep"]
            for prep in preps:
                orgs = [token for token in prep.children if token.ent_type_ == "ORG"]
                print({'person': ent, 'orgs': orgs, 'past': head.tag_ == "VBD"})
    return doc

# To make the entities easier to work with, we'll merge them into single tokens
nlp.add_pipe(merge_entities)
nlp.add_pipe(extract_person_orgs)

doc = nlp("Alex Smith worked at Acme Corp Inc.")
# If you're not in a Jupyter / IPython environment, use displacy.serve
displacy.render(doc, options={'fine_grained': True})
```

If you change the sentence structure above, for example to “was working”, you’ll notice that our current logic fails and doesn’t correctly detect the company as a past organization. That’s because the root is a participle and the tense information is in the attached auxiliary “was”. To solve this, we can adjust the rules to also check for the above construction:

```
def extract_person_orgs(doc):
    person_entities = [ent for ent in doc.ents if ent.label_ == "PERSON"]
    for ent in person_entities:
        head = ent.root.head
        if head.lemma_ == "work":
            preps = [token for token in head.children if token.dep_ == "prep"]
            for prep in preps:
                orgs = [t for t in prep.children if t.ent_type_ == "ORG"]
                aux = [token for token in head.children if token.dep_ == "aux"]
                past_aux = any(t.tag_ == "VBD" for t in aux)
                past = head.tag_ == "VBD" or head.tag_ == "VBG" and past_aux
                print({'person': ent, 'orgs': orgs, 'past': past})
    return doc
```

In your final rule-based system, you may end up with **several different code paths** to cover the types of constructions that occur in your data.





