<br/>

If you need to match large terminology lists, you can also use the [PhraseMatcher](https://spacy.io/api/phrasematcher) and create [Doc](https://spacy.io/api/doc) objects instead of token patterns, which is much more efficient overall. The `Doc` patterns can contain single or multiple tokens.

## Adding phrase patterns

```
from spacy.matcher import PhraseMatcher

nlp = spacy.load('en_core_web_sm')
matcher = PhraseMatcher(nlp.vocab)
terms = ["Barack Obama", "Angela Merkel", "Washington, D.C."]
# Only run nlp.make_doc to speed things up
patterns = [nlp.make_doc(text) for text in terms]
matcher.add("TerminologyList", None, *patterns)

doc = nlp("German Chancellor Angela Merkel and US President Barack Obama "
          "converse in the Oval Office inside the White House in Washington, D.C.")
matches = matcher(doc)
for match_id, start, end in matches:
    span = doc[start:end]
    print(span.text)
```

Since spaCy is used for processing both the patterns and the text to be matched, you won’t have to worry about specific tokenization – for example, you can simply pass in `nlp("Washington, D.C.")` and won’t have to write a complex token pattern covering the exact tokenization of the term.

> #### Important note on creating patterns
> To create the patterns, each phrase has to be processed with the `nlp` object. If you have a model loaded, doing this in a loop or list comprehension can easily become inefficient and slow. If you **only need the tokenization and lexical attributes**, you can run [nlp.make_doc](https://spacy.io/api/language#make_doc) instead, which will only run the tokenizer. For an additional speed boost, you can also use the [nlp.tokenizer.pipe](https://spacy.io/api/tokenizer#pipe) method, which will process the texts as a stream.<br>
> `-` patterns = [nlp(term) for term in LOTS_OF_TERMS]<br/>
> `+` patterns = [nlp.make_doc(term) for term in LOTS_OF_TERMS]<br/>
> `+` patterns = list(nlp.tokenizer.pipe(LOTS_OF_TERMS))

## Matching on other token attributes 

By default, the `PhraseMatcher` will match on the verbatim token text, e.g. `Token.text`. By setting the `attr` argument on initialization, you can change **which token attribute the matcher should use** when comparing the phrase pattern to the matched Doc. For example, using the attribute `LOWER` lets you match on `Token.lower` and create case-insensitive match patterns:

```
from spacy.lang.en import English
from spacy.matcher import PhraseMatcher

nlp = English()
matcher = PhraseMatcher(nlp.vocab, attr="LOWER")
patterns = [nlp.make_doc(name) for name in ["Angela Merkel", "Barack Obama"]]
matcher.add("Names", None, *patterns)

doc = nlp("angela merkel and us president barack Obama")
for match_id, start, end in matcher(doc):
    print("Matched based on lowercase token text:", doc[start:end])
```

> #### Important note on creating patterns
> The examples here use [nlp.make_doc](https://spacy.io/api/language#make_doc) to create Doc object patterns as efficiently as possible and without running any of the other pipeline components. If the token attribute you want to match on are set by a pipeline component, **make sure that the pipeline component runs** when you create the pattern. For example, to match on `POS` or `LEMMA`, the pattern `Doc` objects need to have part-of-speech tags set by the `tagger`. You can either call the `nlp` object on your pattern texts instead of `nlp.make_doc`, or use [nlp.disable_pipes](https://spacy.io/api/language#disable_pipes) to disable components selectively.

<br/>

Another possible use case is matching number tokens like IP addresses based on their shape. This means that you won’t have to worry about how those string will be tokenized and you’ll be able to find tokens and combinations of tokens based on a few examples. Here, we’re matching on the shapes `ddd.d.d.d` and `ddd.ddd.d.d`:

```
from spacy.lang.en import English
from spacy.matcher import PhraseMatcher

nlp = English()
matcher = PhraseMatcher(nlp.vocab, attr="SHAPE")
matcher.add("IP", None, nlp("127.0.0.1"), nlp("127.127.0.0"))

doc = nlp("Often the router will have an IP address such as 192.168.1.1 or 192.168.2.1.")
for match_id, start, end in matcher(doc):
    print("Matched based on token shape:", doc[start:end])
```

In theory, the same also works for attributes like `POS`. For example, a pattern `nlp("I like cats")` matched based on its part-of-speech tag would return a match for “I love dogs”. You could also match on boolean flags like `IS_PUNCT` to match phrases with the same sequence of punctuation and non-punctuation tokens as the pattern. But this can easily get confusing and doesn’t have much of an advantage over writing one or two token patterns.







