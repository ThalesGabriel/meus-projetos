<br/>

spaCy makes it very easy to create your own pipelines consisting of reusable components – this includes spaCy’s default tagger, parser and entity recognizer, but also your own custom processing functions. A pipeline component can be added to an already existing `nlp` object, specified when initializing a `Language` class, or defined within a [model package](https://spacy.io/usage/saving-loading#models).

<br/>

When you load a model, spaCy first consults the model’s [meta.json](https://spacy.io/usage/saving-loading#models). The meta typically includes the model details, the ID of a language class, and an optional list of pipeline components. spaCy then does the following:

1. Load the **language class and data** for the given ID via [get_lang_class](https://spacy.io/api/top-level#util.get_lang_class) and initialize it. The Language class contains the shared vocabulary, tokenization rules and the language-specific annotation scheme.
2. Iterate over the **pipeline names** and create each component using [create_pipe](https://spacy.io/api/language#create_pipe), which looks them up in Language.factories.
3. Add each pipeline component to the pipeline in order, using [add_pipe](https://spacy.io/api/language#add_pipe).
4. Make the **model data** available to the Language class by calling [from_disk](https://spacy.io/api/language#from_disk) with the path to the model data directory.

<br/>

So when you call this…

```nlp = spacy.load("en_core_web_sm")```

… the model’s `meta.json` tells spaCy to use the language `"en"` and the pipeline `["tagger", "parser", "ner"]`. spaCy will then initialize `spacy.lang.en.English`, and create each pipeline component and add it to the processing pipeline. It’ll then load in the model’s data from its data directory and return the modified `Language` class for you to use as the `nlp` object.

<br/>

Fundamentally, a [spaCy model](https://spacy.io/models) consists of three components: **the weights**, i.e. binary data loaded in from a directory, a **pipeline** of functions called in order, and **language data** like the tokenization rules and annotation scheme. All of this is specific to each model, and defined in the model’s meta.json – for example, a Spanish NER model requires different weights, language data and pipeline components than an English parsing and tagging model. This is also why the pipeline state is always held by the `Language` class. [spacy.load](https://spacy.io/api/top-level#spacy.load) puts this all together and returns an instance of `Language` with a pipeline set and access to the binary data:

```
lang = "en"
pipeline = ["tagger", "parser", "ner"]
data_path = "path/to/en_core_web_sm/en_core_web_sm-2.0.0"

cls = spacy.util.get_lang_class(lang)   # 1. Get Language instance, e.g. English()
nlp = cls()                             # 2. Initialize it
for name in pipeline:
    component = nlp.create_pipe(name)   # 3. Create the pipeline components
    nlp.add_pipe(component)             # 4. Add the component to the pipeline
nlp.from_disk(model_data_path)          # 5. Load in the binary data
```

When you call nlp on a text, spaCy will **tokenize** it and then **call each component** on the `Doc`, in order. Since the model data is loaded, the components can access it to assign annotations to the `Doc` object, and subsequently to the `Token` and `Span` which are only views of the `Doc`, and don’t own any data themselves. All components return the modified document, which is then processed by the component next in the pipeline.

```
doc = nlp.make_doc("This is a sentence")   # create a Doc from raw text
for name, proc in nlp.pipeline:             # iterate over components in order
    doc = proc(doc)                         # apply each component
```

The current processing pipeline is available as `nlp.pipeline`, which returns a list of `(name, component)` tuples, or `nlp.pipe_names`, which only returns a list of human-readable component names.

```
print(nlp.pipeline)
# [('tagger', <spacy.pipeline.Tagger>), ('parser', <spacy.pipeline.DependencyParser>), ('ner', <spacy.pipeline.EntityRecognizer>)]
print(nlp.pipe_names)
# ['tagger', 'parser', 'ner']
```