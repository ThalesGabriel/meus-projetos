<br/>

spaCy ships with several built-in pipeline components that are also available in the `Language.factories`. This means that you can initialize them by calling [nlp.create_pipe](https://spacy.io/api/language#create_pipe) with their string names and require them in the pipeline settings in your model’s meta.json.

|STRING NAME | 	COMPONENT	|   DESCRIPTION
|:-----------:|:------------:|:-------------:|
tagger  | 	[Tagger](https://spacy.io/api/tagger)	| Assign part-of-speech-tags.
parser	| [DependencyParser](https://spacy.io/api/dependencyparser) |	Assign dependency labels.
ner	     |[EntityRecognizer](https://spacy.io/api/entityrecognizer|	Assign named entities.
entity_linker  |	[EntityLinker](https://spacy.io/api/entitylinker) | 	Assign knowledge base IDs to named entities. Should be added after the entity recognizer.
textcat	  | [TextCategorizer](https://spacy.io/api/textcategorizer)	 | Assign text categories.
entity_ruler |	[EntityRuler](https://spacy.io/api/entityruler)	 | Assign named entities based on pattern rules.
sentencizer	 |[Sentencizer](https://spacy.io/api/sentencizer)	| Add rule-based sentence segmentation without the dependency parse.
merge_noun_chunks |	[merge_noun_chunks](https://spacy.io/api/sentencizer)	 | Merge all noun chunks into a single token. Should be added after the tagger and parser.
merge_entities |	[merge_entities](https://spacy.io/api/pipeline-functions#merge_entities) |	Merge all entities into a single token. Should be added after the entity recognizer.
merge_subtokens	 | [merge_subtokens](https://spacy.io/api/pipeline-functions#merge_subtokens) |	Merge subtokens predicted by the parser into single tokens. Should be added after the parser.

## Disabling and modifying pipeline components

If you don’t need a particular component of the pipeline – for example, the tagger or the parser, you can **disable loading** it. This can sometimes make a big difference and improve loading speed. Disabled component names can be provided to [spacy.load](https://spacy.io/api/top-level#spacy.load), [Language.from_disk](https://spacy.io/api/language#from_disk) or the `nlp` object itself as a list:

```
nlp = spacy.load("en_core_web_sm", disable=["tagger", "parser"])
nlp = English().from_disk("/model", disable=["ner"])
```

In some cases, you do want to load all pipeline components and their weights, because you need them at different points in your application. However, if you only need a `Doc` object with named entities, there’s no need to run all pipeline components on it – that can potentially make processing much slower. Instead, you can use the `disable` keyword argument on [nlp.pipe](https://spacy.io/api/language#pipe) to temporarily disable the components **during processing**:

```
for doc in nlp.pipe(texts, disable=["tagger", "parser"]):
    # Do something with the doc here
```

If you need to **execute more code** with components disabled – e.g. to reset the weights or update only some components during training – you can use the [nlp.disable_pipes](https://spacy.io/api/language#disable_pipes) contextmanager. At the end of the `with` block, the disabled pipeline components will be restored automatically. Alternatively, `disable_pipes` returns an object that lets you call its `restore()` method to restore the disabled components when needed. This can be useful if you want to prevent unnecessary code indentation of large blocks.

```
# 1. Use as a contextmanager
with nlp.disable_pipes("tagger", "parser"):
    doc = nlp("I won't be tagged and parsed")
doc = nlp("I will be tagged and parsed")

# 2. Restore manually
disabled = nlp.disable_pipes("ner")
doc = nlp("I won't have named entities")
disabled.restore()
```

Finally, you can also use the [remove_pipe](https://spacy.io/api/language#remove_pipe) method to remove pipeline components from an existing pipeline, the [rename_pipe](https://spacy.io/api/language#rename_pipe) method to rename them, or the [replace_pipe](https://spacy.io/api/language#replace_pipe) method to replace them with a custom component entirely (more details on this in the section on [custom components](https://spacy.io/usage/processing-pipelines#custom-components).

```
nlp.remove_pipe("parser")
nlp.rename_pipe("ner", "entityrecognizer")
nlp.replace_pipe("tagger", my_custom_tagger)
```

> #### Important note: disabling pipeline components
> Since spaCy v2.0 comes with better support for customizing the processing pipeline components, the `parser`, `tagger` and `entity` keyword arguments have been replaced with `disable`, which takes a list of pipeline component names. This lets you disable pre-defined components when loading a model, or initializing a Language class via [from_disk](https://spacy.io/api/language#from_disk).
> 
> ```- nlp = spacy.load('en', tagger=False, entity=False)```<br/>
>``` - doc = nlp("I don't want parsed", parse=False)```
> <br><br/>
> ```+ nlp = spacy.load("en", disable=["ner"])```<br/>
> ```+ nlp.remove_pipe("parser")```<br/>
> ```+ doc = nlp("I don't want parsed")```

