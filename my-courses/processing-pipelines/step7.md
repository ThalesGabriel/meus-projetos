<br/>

Collecting training data may sound incredibly painful – and it can be, if you’re planning a large-scale annotation project. However, if your main goal is to update an existing model’s predictions – for example, spaCy’s named entity recognition – the hard part is usually not creating the actual annotations. It’s finding representative examples and **extracting potential candidates**. The good news is, if you’ve been noticing bad performance on your data, you likely already have some relevant text, and you can use spaCy to **bootstrap a first set of training examples**.

| TEXT	|  ENTITY | 	START | 	END	 | LABEL	|   |
|:------:|:------:|:------:|:------:|:------:|:------:|
Uber blew through 1 million a week	| Uber |	0 | 4 | ORG	| Right
Android Pay expands to Canada	| Android	| 0	 |7	  | PERSON | Wrong	
Android Pay expands to Canada	| Canada	| 23 | 	30 | 	GPE	| Right
Spotify steps up Asia expansion	 |Spotify	 | 0	| 8	 |ORG	| Right
Spotify steps up Asia expansion	 | Asia	| 17 | 	21	| NORP	| Wrong


Alternatively, the [rule-based matcher](https://spacy.io/usage/rule-based-matching) can be a useful tool to extract tokens or combinations of tokens, as well as their start and end index in a document. In this case, we’ll extract mentions of Google and assume they’re an `ORG`.

| TEXT	|  ENTITY | 	START | 	END	 | LABEL	|   |
|:------:|:------:|:------:|:------:|:------:|:------:|
let me google this for you | google |	7 | 13 | ORG	| Wrong
Google Maps launches location sharing	| Google	| 0	 |6	  | PERSON | Wrong	
Google rebrands its business apps	| Google	| 0 | 	6 | 	GPE	| Right
look what i found on google! 😂 |google	 | 21	| 27	 |ORG	| Right

Based on the few examples above, you can already create six training sentences with eight entities in total. Of course, what you consider a “correct annotation” will always depend on **what you want the model to learn**. While there are some entity annotations that are more or less universally correct – like Canada being a geopolitical entity – your application may have its very own definition of the [NER annotation scheme](https://spacy.io/api/annotation#named-entities).

```
train_data = [
    ("Uber blew through $1 million a week", [(0, 4, 'ORG')]),
    ("Android Pay expands to Canada", [(0, 11, 'PRODUCT'), (23, 30, 'GPE')]),
    ("Spotify steps up Asia expansion", [(0, 8, "ORG"), (17, 21, "LOC")]),
    ("Google Maps launches location sharing", [(0, 11, "PRODUCT")]),
    ("Google rebrands its business apps", [(0, 6, "ORG")]),
    ("look what i found on google! 😂", [(21, 27, "PRODUCT")])]
```

> #### Tip: Try the Prodigy annotation tool
> If you need to label a lot of data, check out [Prodigy](https://prodi.gy/), a new, active learning-powered annotation tool we’ve developed. Prodigy is fast and extensible, and comes with a modern **web application** that helps you collect training data faster. It integrates seamlessly with spaCy, pre-selects the **most relevant examples** for annotation, and lets you train and evaluate ready-to-use spaCy models.

## Training with annotations

The [GoldParse](https://spacy.io/api/goldparse) object collects the annotated training examples, also called the **gold standard**. It’s initialized with the [Doc](https://spacy.io/api/doc) object it refers to, and keyword arguments specifying the annotations, like `tags` or `entities`. Its job is to encode the annotations, keep them aligned and create the C-level data structures required for efficient access. Here’s an example of a simple `GoldParse` for part-of-speech tags:

```
vocab = Vocab(tag_map={"N": {"pos": "NOUN"}, "V": {"pos": "VERB"}})
doc = Doc(vocab, words=["I", "like", "stuff"])
gold = GoldParse(doc, tags=["N", "V", "N"])
```

Using the `Doc` and its gold-standard annotations, the model can be updated to learn a sentence of three words with their assigned part-of-speech tags. The [tag map](https://spacy.io/usage/adding-languages#tag-map) is part of the vocabulary and defines the annotation scheme. If you’re training a new language model, this will let you map the tags present in the treebank you train on to spaCy’s tag scheme.

```
doc = Doc(Vocab(), words=["Facebook", "released", "React", "in", "2014"])
gold = GoldParse(doc, entities=["U-ORG", "O", "U-TECHNOLOGY", "O", "U-DATE"])
```

The same goes for named entities. The letters added before the labels refer to the tags of the [BILUO scheme](https://spacy.io/usage/linguistic-features#updating-biluo) – `O` is a token outside an entity, `U` an single entity unit, `B` the beginning of an entity, `I` a token inside an entity and `L` the last token of an entity.

<br/>

Of course, it’s not enough to only show a model a single example once. Especially if you only have few examples, you’ll want to train for a **number of iterations**. At each iteration, the training data is **shuffled** to ensure the model doesn’t make any generalizations based on the order of examples. Another technique to improve the learning results is to set a **dropout rate**, a rate at which to randomly “drop” individual features and representations. This makes it harder for the model to memorize the training data. For example, a `0.25` dropout means that each feature or internal representation has a 1/4 likelihood of being dropped.

```
optimizer = nlp.begin_training(get_data)
for itn in range(100):
    random.shuffle(train_data)
    for raw_text, entity_offsets in train_data:
        doc = nlp.make_doc(raw_text)
        gold = GoldParse(doc, entities=entity_offsets)
        nlp.update([doc], [gold], drop=0.5, sgd=optimizer)
nlp.to_disk("/model")
```

The [nlp.update](https://spacy.io/api/language#update) method takes the following arguments:

| NAME	|  DESCRIPTION |
|:-----:|:------------:|
docs | 	[Doc](https://spacy.io/api/doc) objects. The update method takes a sequence of them, so you can batch up your training examples. Alternatively, you can also pass in a sequence of raw texts.
golds | 	[GoldParse](https://spacy.io/api/goldparse) objects. The update method takes a sequence of them, so you can batch up your training examples. Alternatively, you can also pass in a dictionary containing the annotations.
drop | 	Dropout rate. Makes it harder for the model to just memorize the data.
sgd	 | An optimizer, i.e. a callable to update the model’s weights. If not set, spaCy will create a new one and save it for further use.


Instead of writing your own training loop, you can also use the built-in [train](https://spacy.io/api/cli#train) command, which expects data in spaCy’s [JSON format](https://spacy.io/api/annotation#json-input). On each epoch, a model will be saved out to the directory. After training, you can use the [package](https://spacy.io/api/cli#package) command to generate an installable Python package from your model.

```
python -m spacy convert /tmp/train.conllu /tmp/data
python -m spacy train en /tmp/model /tmp/data/train.json -n 5
```

