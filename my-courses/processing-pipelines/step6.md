<br/>

This guide describes how to train new statistical models for spaCy’s part-of-speech tagger, named entity recognizer, dependency parser, text classifier and entity linker. Once the model is trained, you can then [save and load it](https://spacy.io/usage/saving-loading#models).

## Training basics

spaCy’s models are **statistical** and every “decision” they make – for example, which part-of-speech tag to assign, or whether a word is a named entity – is a prediction. This **prediction** is based on the examples the model has seen during **training**. To train a model, you first need training data – examples of text, and the labels you want the model to predict. This could be a part-of-speech tag, a named entity or any other information.

<br/>

The model is then shown the unlabelled text and will make a prediction. Because we know the correct answer, we can give the model feedback on its prediction in the form of an **error gradient** of the **loss function** that calculates the difference between the training example and the expected output. The greater the difference, the more significant the gradient and the updates to our model.

<br/>

When training a model, we don’t just want it to memorize our examples – we want it to come up with a theory that can be **generalized across other examples**. After all, we don’t just want the model to learn that this one instance of “Amazon” right here is a company – we want it to learn that “Amazon”, in contexts like this, is most likely a company. That’s why the training data should always be representative of the data we want to process. A model trained on Wikipedia, where sentences in the first person are extremely rare, will likely perform badly on Twitter. Similarly, a model trained on romantic novels will likely perform badly on legal text.

<br/>

This also means that in order to know how the model is performing, and whether it’s learning the right things, you don’t only need **training data** – you’ll also need **evaluation data**. If you only test the model with the data it was trained on, you’ll have no idea how well it’s generalizing. If you want to train a model from scratch, you usually need at least a few hundred examples for both training and evaluation. To update an existing model, you can already achieve decent results with very few examples – as long as they’re representative.

## Training via the command-line interface

For most purposes, the best way to train spaCy is via the command-line interface. The [spacy train](https://spacy.io/api/cli#train) command takes care of many details for you, including making sure that the data is minibatched and shuffled correctly, progress is printed, and models are saved after each epoch. You can prepare your data for use in spacy train using the [spacy convert](https://spacy.io/api/cli#convert) command, which accepts many common NLP data formats, including .iob for named entities, and the CoNLL format for dependencies:

```
git clone https://github.com/UniversalDependencies/UD_Spanish-AnCora
mkdir ancora-json
python -m spacy convert UD_Spanish-AnCora/es_ancora-ud-train.conllu ancora-json
python -m spacy convert UD_Spanish-AnCora/es_ancora-ud-dev.conllu ancora-json
mkdir models
python -m spacy train es models ancora-json/es_ancora-ud-train.json ancora-json/es_ancora-ud-dev.json
```

> #### Tip: Debug your data
> If you’re running spaCy v2.2 or above, you can use the [debug-data command](https://spacy.io/api/cli#debug-data) to analyze and validate your training and development data, get useful stats, and find problems like invalid entity annotations, cyclic dependencies, low data labels and more.
> ```$ python -m spacy debug-data en train.json dev.json --verbose```

You can also use the [gold.docs_to_json](https://spacy.io/api/goldparse#docs_to_json) helper to convert a list of Doc objects to spaCy’s JSON training format.

## Understanding the training output

When you train a model using the [spacy train](https://spacy.io/api/cli#train) command, you’ll see a table showing metrics after each pass over the data. Here’s what those metrics means:

| NAME  |	DESCRIPTION  |
|:------:|:-------------:|
Dep Loss	| Training loss for dependency parser. Should decrease, but usually not to 0.
NER Loss	| Training loss for named entity recognizer. Should decrease, but usually not to 0.
UAS |	Unlabeled attachment score for parser. The percentage of unlabeled correct arcs. Should increase.
NER P. |	NER precision on development data. Should increase.
NER R. |	NER recall on development data. Should increase.
NER F. |	NER F-score on development data. Should increase.
Tag % |	Fine-grained part-of-speech tag accuracy on development data. Should increase.
Token % |	Tokenization accuracy on development data.
CPU WPS |	Prediction speed on CPU in words per second, if available. Should stay stable.
GPU WPS | Prediction speed on GPU in words per second, if available. Should stay stable.

## Improving accuracy with transfer learning 

In most projects, you’ll usually have a small amount of labelled data, and access to a much bigger sample of raw text. The raw text contains a lot of information about the language in general. Learning this general information from the raw text can help your model use the smaller labelled data more efficiently.

<br/>

The two main ways to use raw text in your spaCy models are **word vectors** and **language model pretraining**. Word vectors provide information about the definitions of words. The vectors are a look-up table, so each word only has one representation, regardless of its context. Language model pretraining lets you learn contextualized word representations. Instead of initializing spaCy’s convolutional neural network layers with random weights, the spacy pretrain command trains a language model to predict each word’s word vector based on the surrounding words. The information used to predict this task is a good starting point for other tasks such as named entity recognition, text classification or dependency parsing.

> 📖 Vectors and pretraining
> For more details, see the documentation on [vectors and similarity](https://spacy.io/usage/vectors-similarity) and the [spacy pretrain](https://spacy.io/api/cli#pretrain) command.









