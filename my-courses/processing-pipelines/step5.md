<br/>

Word vectors let you import knowledge from raw text into your model. The knowledge is represented as a table of numbers, with one row per term in your vocabulary. If two terms are used in similar contexts, the algorithm that learns the vectors should assign them **rows that are quite similar**, while words that are used in different contexts will have quite different values. This lets you use the row-values assigned to the words as a kind of dictionary, to tell you some things about what the words in your text mean.


<br/>


Word vectors are particularly useful for terms which **aren’t well represented in your labelled training data**. For instance, if you’re doing named entity recognition, there will always be lots of names that you don’t have examples of. For instance, imagine your training data happens to contain some examples of the term “Microsoft”, but it doesn’t contain any examples of the term “Symantec”. In your raw text sample, there are plenty of examples of both terms, and they’re used in similar contexts. The word vectors make that fact available to the entity recognition model. It still won’t see examples of “Symantec” labelled as a company. However, it’ll see that “Symantec” has a word vector that usually corresponds to company terms, so it can **make the inference**.

<br/>

In order to make best use of the word vectors, you want the word vectors table to cover a **very large vocabulary**. However, most words are rare, so most of the rows in a large word vectors table will be accessed very rarely, or never at all. You can usually cover more than **95% of the tokens** in your corpus with just **a few thousand rows** in the vector table. However, it’s those **5% of rare terms** where the word vectors are **most useful**. The problem is that increasing the size of the vector table produces rapidly diminishing returns in coverage over these rare terms.


## Converting word vectors for use in spaCy 

Custom word vectors can be trained using a number of open-source libraries, such as [Gensim](https://radimrehurek.com/gensim), [Fast Text](https://fasttext.cc/), or Tomas Mikolov’s original [word2vec implementation](https://code.google.com/archive/p/word2vec/). Most word vector libraries output an easy-to-read text-based format, where each line consists of the word followed by its vector. For everyday use, we want to convert the vectors model into a binary format that loads faster and takes up less space on disk. The easiest way to do this is the [init-model](https://spacy.io/api/cli#init-model)command-line utility:

```
wget https://s3-us-west-1.amazonaws.com/fasttext-vectors/word-vectors-v2/cc.la.300.vec.gz
python -m spacy init-model en /tmp/la_vectors_wiki_lg --vectors-loc cc.la.300.vec.gz
```

This will output a spaCy model in the directory `/tmp/la_vectors_wiki_lg`, giving you access to some nice Latin vectors You can then pass the directory path to [spacy.load()](https://spacy.io/api/top-level#spacy.load).

```
nlp_latin = spacy.load("/tmp/la_vectors_wiki_lg")
doc1 = nlp_latin("Caecilius est in horto")
doc2 = nlp_latin("servus est in atrio")
doc1.similarity(doc2)
```

The model directory will have a `/vocab` directory with the strings, lexical entries and word vectors from the input vectors model. The [init-model](https://spacy.io/api/cli#init-model) command supports a number of archive formats for the word vectors: the vectors can be in plain text (`.txt`), zipped (`.zip`), or tarred and zipped (`.tgz`).








