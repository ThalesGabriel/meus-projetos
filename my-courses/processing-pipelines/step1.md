<br/>

When you call `nlp` on a text, spaCy first tokenizes the text to produce a Doc object. The Doc is then processed in several different steps – this is also referred to as the **processing pipeline**. The pipeline used by the [default models](https://spacy.io/models) consists of a tagger, a parser and an entity recognizer. Each pipeline component returns the processed Doc, which is then passed on to the next component.


|   NAME   | COMPONENT  |	CREATES	  |  DESCRIPTION  |
|:--------:|:----------:|:-----------:|:-------------:|
tokenizer  | [Tokenizer](https://spacy.io/api/tokenizer)  |	Doc       |	Segment text into tokens.
tagger	   | [Tagger](https://spacy.io/api/tagger)     |	Doc[i].tag |	Assign part-of-speech tags.
parser	   | [DependencyParser](https://spacy.io/api/dependencyparser) |	Doc[i].head, Doc[i].dep, Doc.sents, Doc.noun_chunks	|Assign dependency labels.
ner	       | [EntityRecognizer](https://spacy.io/api/entityrecognizer) |	Doc.ents, Doc[i].ent_iob, Doc[i].ent_type	| Detect and label named entities.
textcat	   | [TextCategorizer](https://spacy.io/api/textcategorizer)  |	Doc.cats	| Assign document labels.
…	       | [custom components](https://spacy.io/usage/processing-pipelines#custom-components) |	Doc._.xxx, Token._.xxx, Span._.xxx	| Assign custom attributes, methods or properties.

The processing pipeline always **depends on the statistical model** and its capabilities. For example, a pipeline can only include an entity recognizer component if the model includes data to make predictions of entity labels. This is why each model will specify the pipeline to use in its meta data, as a simple list containing the component names:

```"pipeline": ["tagger", "parser", "ner"]```

## Processing text

When you call nlp on a text, spaCy will **tokenize** it and then **call each component** on the `Doc`, in order. It then returns the processed `Doc` that you can work with.

```doc = nlp("This is a text")```

When processing large volumes of text, the statistical models are usually more efficient if you let them work on batches of texts. spaCy’s [nlp.pipe](https://spacy.io/api/language#pipe) method takes an iterable of texts and yields processed `Doc` objects. The batching is done internally.

```
texts = ["This is a text", "These are lots of texts", "..."]
- docs = [nlp(text) for text in texts]
+ docs = list(nlp.pipe(texts))
```

> #### Tips for efficient processing
> - Process the texts as a stream using [nlp.pipe](https://spacy.io/api/language#pipe) and buffer them in batches, instead of one-by-one. This is usually much more efficient.<br/>
> - Only apply the **pipeline components you need**. Getting predictions from the model that you don’t actually need adds up and becomes very inefficient at scale. To prevent this, use the disable keyword argument to disable components you don’t need – either when loading a model, or during processing with nlp.pipe. See the section on [disabling pipeline components](https://spacy.io/usage/processing-pipelines#disabling) for more details and examples.

In this example, we’re using [nlp.pipe](https://spacy.io/api/language#pipe) to process a (potentially very large) iterable of texts as a stream. Because we’re only accessing the named entities in `doc.ents` (set by the ner component), we’ll disable all other statistical components (the `tagger` and `parser`) during processing. `nlp.pipe` yields `Doc` objects, so we can iterate over them and access the named entity predictions:

```
import spacy

texts = [
    "Net income was $9.4 million compared to the prior year of $2.7 million.",
    "Revenue exceeded twelve billion dollars, with a loss of $1b.",
]

nlp = spacy.load("en_core_web_sm")
for doc in nlp.pipe(texts, disable=["tagger", "parser"]):
    # Do something with the doc here
    print([(ent.text, ent.label_) for ent in doc.ents])
```

> #### Important note
> When using [nlp.pipe](https://spacy.io/api/language#pipe), keep in mind that it returns a [generator](https://realpython.com/introduction-to-python-generators/) that yields `Doc` objects – not a list. So if you want to use it like a list, you’ll have to call `list()` on it first:<br/>
> ```- docs = nlp.pipe(texts)[0]         # will raise an error```<br/>
> ```+ docs = list(nlp.pipe(texts))[0]   # works as expected```

