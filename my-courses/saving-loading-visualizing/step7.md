<br>

Rendering several large documents on one page can easily become confusing. To add a headline to each visualization, you can add a `title` to its `user_data`. User data is never touched or modified by spaCy.

```
doc = nlp("This is a sentence about Google.")
doc.user_data["title"] = "This is a title"
displacy.serve(doc, style="ent")
```

This feature is especially handy if you’re using displaCy to compare performance at different stages of a process, e.g. during training. Here you could use the title for a brief description of the text example and the number of iterations.

