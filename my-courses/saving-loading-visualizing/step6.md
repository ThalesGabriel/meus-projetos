<br/>

Long texts can become difficult to read when displayed in one row, so it’s often better to visualize them sentence-by-sentence instead. As of v2.0.12, `displacy` supports rendering both [Doc](https://spacy.io/api/doc) and [Span](https://spacy.io/api/span) objects, as well as lists of `Docs` or `Spans`. Instead of passing the full Doc to displacy.serve, you can also pass in a list `doc.sents`. This will create one visualization for each sentence.

```
import spacy
from spacy import displacy

nlp = spacy.load("en_core_web_sm")
text = """In ancient Rome, some neighbors live in three adjacent houses. In the center is the house of Senex, who lives there with wife Domina, son Hero, and several slaves, including head slave Hysterium and the musical's main character Pseudolus. A slave belonging to Hero, Pseudolus wishes to buy, win, or steal his freedom. One of the neighboring houses is owned by Marcus Lycus, who is a buyer and seller of beautiful women; the other belongs to the ancient Erronius, who is abroad searching for his long-lost children (stolen in infancy by pirates). One day, Senex and Domina go on a trip and leave Pseudolus in charge of Hero. Hero confides in Pseudolus that he is in love with the lovely Philia, one of the courtesans in the House of Lycus (albeit still a virgin)."""
doc = nlp(text)
sentence_spans = list(doc.sents)
displacy.serve(sentence_spans, style="dep")
```

## Visualizing the entity recognizer

The entity visualizer, ent, highlights named entities and their labels in a text.

```
import spacy
from spacy import displacy

text = "When Sebastian Thrun started working on self-driving cars at Google in 2007, few people outside of the company took him seriously."

nlp = spacy.load("en_core_web_sm")
doc = nlp(text)
displacy.serve(doc, style="ent")
```

The entity visualizer lets you customize the following options:


ARGUMENT | 	TYPE	| DESCRIPTION	| DEFAULT|
|:------:|:--------:|:-------------:|:------:|
ents	 | list	    |   Entity types to highlight (None for all types). | 	None
colors   |	dict	| Color overrides. Entity types in uppercase should be mapped to color names or values.	| {}

If you specify a list of `ents`, only those entity types will be rendered – for example, you can choose to display `PERSON` entities. Internally, the visualizer knows nothing about available entity types and will render whichever spans and labels it receives. This makes it especially easy to work with custom entity types. By default, displaCy comes with colors for all [entity types supported by spaCy](https://spacy.io/api/annotation#named-entities). If you’re using custom entity types, you can use the colors setting to add your own colors for them.

<br/>

The above example uses a little trick: Since the background color values are added as the background style attribute, you can use any [valid background value](https://tympanus.net/codrops/css_reference/background/) or shorthand — including gradients and even images!



