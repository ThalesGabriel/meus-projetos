<br/>

When pickling spaCy’s objects like the [Doc](https://spacy.io/api/doc) or the [EntityRecognizer](https://spacy.io/api/entityrecognizer), keep in mind that they all require the shared [Vocab](https://spacy.io/api/vocab) (which includes the string to hash mappings, label schemes and optional vectors). This means that their pickled representations can become very large, especially if you have word vectors loaded, because it won’t only include the object itself, but also the entire shared vocab it depends on.

<br/>

If you need to pickle multiple objects, try to pickle them **together** instead of separately. For instance, instead of pickling all pipeline components, pickle the entire pipeline once. And instead of pickling several Doc objects separately, pickle a list of Doc objects. Since the all share a reference to the same Vocab object, it will only be included once.

```
doc1 = nlp("Hello world")
doc2 = nlp("This is a test")

doc1_data = pickle.dumps(doc1)
doc2_data = pickle.dumps(doc2)
print(len(doc1_data) + len(doc2_data))  # 6636116 😞

doc_data = pickle.dumps([doc1, doc2])
print(len(doc_data))  # 3319761 😃
```

> #### Pickling spans and tokens
> Pickling `Token` and `Span` objects isn’t supported. They’re only views of the `Doc` and can’t exist on their own. Pickling them would always mean pulling in the parent document and its vocabulary, which has practically no advantage over pickling the parent `Doc`.<br/><br/>
>`- data = pickle.dumps(doc[10:20])` <br/>
>`+ data = pickle.dumps(doc)`<br/><br/>
> If you really only need a span – for example, a particular sentence – you can use [Span.as_doc](https://spacy.io/api/span#as_doc) to make a copy of it and convert it to a `Doc` object. However, note that this will not let you recover contextual information from outside the span.<br/><br/>
>`+ span_doc = doc[10:20].as_doc()`<br/>
> `data = pickle.dumps(span_doc)`