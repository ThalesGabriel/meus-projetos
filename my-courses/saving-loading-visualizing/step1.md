<br/>

If you’ve been modifying the pipeline, vocabulary, vectors and entities, or made updates to the model, you’ll eventually want to **save your progress** – for example, everything that’s in your nlp object. This means you’ll have to translate its contents and structure into a format that can be saved, like a file or a byte string. This process is called serialization. spaCy comes with **built-in serialization methods** and supports the [Pickle protocol](https://www.diveinto.org/python3/serializing.html#dump).

<br/>

All container classes, i.e. [Language](https://spacy.io/api/language) `(nlp)`, [Doc](https://spacy.io/api/doc), [Vocab](https://spacy.io/api/vocab) and [StringStore](https://spacy.io/api/stringstore) have the following methods available:

METHOD |	RETURNS |	EXAMPLE
|:-----:|:----------:|:--------:|
to_bytes	| bytes | 	data = nlp.to_bytes()
from_bytes | 	object | 	nlp.from_bytes(data)
to_disk	    | -	| nlp.to_disk("/path")
from_disk	| object | 	nlp.from_disk("/path")

> #### Important note
> In spaCy v2.0, the API for saving and loading has changed to only use the four methods listed above consistently across objects and classes. For an overview of the changes, see [this table](https://spacy.io/usage/v2#incompat) and the notes on [migrating](https://spacy.io/usage/v2#migrating-saving-loading).

## Serializing the pipeline

When serializing the pipeline, keep in mind that this will only save out the **binary data for the individual components** to allow spaCy to restore them – not the entire objects. This is a good thing, because it makes serialization safe. But it also means that you have to take care of storing the language name and pipeline component names as well, and restoring them separately before you can load in the data.

```
bytes_data = nlp.to_bytes()
lang = nlp.meta["lang"]  # "en"
pipeline = nlp.meta["pipeline"]  # ["tagger", "parser", "ner"]
```

<br/>

```
nlp = spacy.blank(lang)
for pipe_name in pipeline:
    pipe = nlp.create_pipe(pipe_name)
    nlp.add_pipe(pipe)
nlp.from_bytes(bytes_data)
```

This is also how spaCy does it under the hood when loading a model: it loads the model’s `meta.json` containing the language and pipeline information, initializes the language class, creates and adds the pipeline components and then loads in the binary data. You can read more about this process [here](https://spacy.io/usage/processing-pipelines#pipelines).