<br/>

When you call [nlp.to_disk](https://spacy.io/api/language#to_disk), [nlp.from_disk](https://spacy.io/api/language#from_disk) or load a model package, spaCy will iterate over the components in the pipeline, check if they expose a `to_disk` or `from_disk` method and if so, call it with the path to the model directory plus the string name of the component. For example, if you’re calling `nlp.to_disk("/path")`, the data for the named entity recognizer will be saved in `/path/ner`.

<br/>

If you’re using custom pipeline components that depend on external data – for example, model weights or terminology lists – you can take advantage of spaCy’s built-in component serialization by making your custom component expose its own `to_disk` and `from_disk` or `to_bytes` and `from_bytes` methods. When an `nlp` object with the component in its pipeline is saved or loaded, the component will then be able to serialize and deserialize itself. The following example shows a custom component that keeps arbitrary JSON-serializable data, allows the user to add to that data and saves and loads the data to and from a JSON file.

```
class CustomComponent(object):
    name = "my_component"

    def __init__(self):
        self.data = []

    def __call__(self, doc):
        # Do something to the doc here
        return doc

    def add(self, data):
        # Add something to the component's data
        self.data.append(data)

    def to_disk(self, path, **kwargs):
        # This will receive the directory path + /my_component
        data_path = path / "data.json"
        with data_path.open("w", encoding="utf8") as f:
            f.write(json.dumps(self.data))

    def from_disk(self, path, **cfg):
        # This will receive the directory path + /my_component
        data_path = path / "data.json"
        with data_path.open("r", encoding="utf8") as f:
            self.data = json.loads(f)
        return self
```

After adding the component to the pipeline and adding some data to it, we can serialize the `nlp` object to a directory, which will call the custom component’s `to_disk` method.

```
nlp = spacy.load("en_core_web_sm")
my_component = CustomComponent()
my_component.add({"hello": "world"})
nlp.add_pipe(my_component)
nlp.to_disk("/path/to/model")
```

The contents of the directory would then look like this. `CustomComponent.to_disk` converted the data to a JSON string and saved it to a file `data.json` in its subdirectory:

<br>

`DIRECTORY STRUCTURE`

```
└── /path/to/model
    ├── my_component     # data serialized by "my_component"
    |   └── data.json
    ├── ner              # data for "ner" component
    ├── parser           # data for "parser" component
    ├── tagger           # data for "tagger" component
    ├── vocab            # model vocabulary
    ├── meta.json        # model meta.json with name, language and pipeline
    └── tokenizer        # tokenization rules
```

When you load the data back in, spaCy will call the custom component’s `from_disk` method with the given file path, and the component can then load the contents of `data.json`, convert them to a Python object and restore the component state. The same works for other types of data, of course – for instance, you could add a [wrapper for a model](https://spacy.io/usage/processing-pipelines#wrapping-models-libraries) trained with a different library like TensorFlow or PyTorch and make spaCy load its weights automatically when you load the model package.

> #### Important note on loading components
> When you load a model from disk, spaCy will check the `"pipeline"` in the model’s `meta.json` and look up the component name in the internal factories. To make sure spaCy knows how to initialize `"my_component"`, you’ll need to add it to the factories:<br/>
> `from spacy.language import Language`<br/>
> `Language.factories["my_component"] = lambda nlp, **cfg: CustomComponent()`
> For more details, see the documentation on [adding factories](https://spacy.io/usage/processing-pipelines#custom-components-factories) or use [entry points](https://spacy.io/usage/saving-loading#entry-points) to make your extension package expose your custom components to spaCy automatically.
