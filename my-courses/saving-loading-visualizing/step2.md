<br/>

If you’re working with lots of data, you’ll probably need to pass analyses between machines, either to use something like [Dask](https://dask.org/) or [Spark](https://spark.apache.org/), or even just to save out work to disk. Often it’s sufficient to use the [Doc.to_array](https://spacy.io/api/doc#to_array) functionality for this, and just serialize the numpy arrays – but other times you want a more general way to save and restore `Doc` objects.

<br/>

The [DocBin](https://spacy.io/api/docbin) class makes it easy to serialize and deserialize a collection of `Doc` objects together, and is much more efficient than calling [Doc.to_bytes](https://spacy.io/api/doc#to_bytes) on each individual `Doc` object. You can also control what data gets saved, and you can merge pallets together for easy map/reduce-style processing.

```
import spacy
from spacy.tokens import DocBin

doc_bin = DocBin(attrs=["LEMMA", "ENT_IOB", "ENT_TYPE"], store_user_data=True)
texts = ["Some text", "Lots of texts...", "..."]
nlp = spacy.load("en_core_web_sm")
for doc in nlp.pipe(texts):
    doc_bin.add(doc)
bytes_data = doc_bin.to_bytes()

# Deserialize later, e.g. in a new process
nlp = spacy.blank("en")
doc_bin = DocBin().from_bytes(bytes_data)
docs = list(doc_bin.get_docs(nlp.vocab))
```

If `store_user_data` is set to `True`, the `Doc.user_data` will be serialized as well, which includes the values of [extension attributes](https://spacy.io/usage/processing-pipelines#custom-components-attributes) (if they’re serializable with msgpack).

> #### Important note on serializing extension attributes
> Including the `Doc.user_data` and extension attributes will only serialize the **values** of the attributes. To restore the values and access them via the `doc._.` property, you need to register the global attribute on the `Doc` again.
> `docs = list(doc_bin.get_docs(nlp.vocab))`<br/>
> `Doc.set_extension("my_custom_attr", default=None)`<br/>
> `print([doc._.my_custom_attr for doc in docs])`

