<br/>

As of v2.0, our popular visualizers, [displaCy](https://explosion.ai/demos/displacy) and [displaCy ENT](https://explosion.ai/demos/displacy-ent) are finally an official part of the library. Visualizing a dependency parse or named entities in a text is not only a fun NLP demo – it can also be incredibly helpful in speeding up development and debugging your code and training process. If you’re running a [Jupyter](https://jupyter.org/) notebook, displaCy will detect this and return the markup in a format [ready to be rendered and exported](https://spacy.io/usage/visualizers#jupyter).

<br/>

The quickest way to visualize `Doc` is to use [displacy.serve](https://spacy.io/api/top-level#displacy.serve). This will spin up a simple web server and let you view the result straight from your browser. displaCy can either take a single Doc or a list of Doc objects as its first argument. This lets you construct them however you like – using any model or modifications you like.

## Visualizing the dependency parse

The dependency visualizer, `dep`, shows part-of-speech tags and syntactic dependencies.

```
import spacy
from spacy import displacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("This is a sentence.")
displacy.serve(doc, style="dep")
```

The argument `options` lets you specify a dictionary of settings to customize the layout, for example:

> #### Important note
> There’s currently a known issue with the `compact` mode for sentences with short arrows and long dependency labels, that causes labels longer than the arrow to wrap. So if you come across this problem, especially when using custom labels, you’ll have to increase the `distance` setting in the `options` to allow longer arcs.

ARGUMENT |	TYPE |	DESCRIPTION	|DEFAULT
|:------:|:------:|:-------------------:|:------:|
compact | 	bool |	“Compact mode” with square arrows that takes up less space.	| False
color | 	unicode |	Text color (HEX, RGB or color names).	| "#000000"
bg	 | unicode |	Background color (HEX, RGB or color names).	| "#ffffff"
font |	unicode	| Font name or font family for all text.	|"Arial"

For a list of all available options, see the [displacy API documentation](https://spacy.io/api/top-level#displacy_options).