<br/>

A [Doc](https://spacy.io/api/doc) object’s sentences are available via the `Doc.sents` property. Unlike other libraries, spaCy uses the dependency parse to determine sentence boundaries. This is usually more accurate than a rule-based approach, but it also means you’ll need a **statistical model** and accurate predictions. If your texts are closer to general-purpose news or web text, this should work well out-of-the-box. For social media or conversational text that doesn’t follow the same rules, your application may benefit from a custom rule-based implementation. You can either use the built-in [Sentencizer](https://spacy.io/api/sentencizer) or plug an entirely custom rule-based function into your [processing pipeline](https://spacy.io/usage/processing-pipelines).

<br/>

spaCy’s dependency parser respects already set boundaries, so you can preprocess your `Doc` using custom rules before it’s parsed. Depending on your text, this may also improve accuracy, since the parser is constrained to predict parses consistent with the sentence boundaries.

## Default: Using the dependency parse 

To view a `Doc’s` sentences, you can iterate over the `Doc.sents`, a generator that yields [Span](https://spacy.io/api/span) objects.

```
import spacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("This is a sentence. This is another sentence.")
for sent in doc.sents:
    print(sent.text)
```

## Rule-based pipeline component

The [Sentencizer](https://spacy.io/api/sentencizer) component is a [pipeline component](https://spacy.io/usage/processing-pipelines) that splits sentences on punctuation like `.`, `!` or `?`. You can plug it into your pipeline if you only need sentence boundaries without the dependency parse.

```
import spacy
from spacy.lang.en import English

nlp = English()  # just the language with no model
sentencizer = nlp.create_pipe("sentencizer")
nlp.add_pipe(sentencizer)
doc = nlp("This is a sentence. This is another sentence.")
for sent in doc.sents:
    print(sent.text)
```

## Custom rule-based strategy

If you want to implement your own strategy that differs from the default rule-based approach of splitting on sentences, you can also create a [custom pipeline component](https://spacy.io/usage/processing-pipelines#custom-components) that takes a Doc object and sets the `Token.is_sent_start` attribute on each individual token. If set to False, the token is explicitly marked as not the start of a sentence. If set to `None` (default), it’s treated as a missing value and can still be overwritten by the parser.

> #### Important note
> To prevent inconsistent state, you can only set boundaries **before** a document is parsed (and `Doc.is_parsed` is `False`). To ensure that your component is added in the right place, you can set `before='parser'` or `first=True` when adding it to the pipeline using [nlp.add_pipe](https://spacy.io/api/language#add_pipe).

Here’s an example of a component that implements a pre-processing rule for splitting on `'...'` tokens. The component is added before the parser, which is then used to further segment the text. That’s possible, because `is_sent_start` is only set to True for some of the tokens – all others still specify `None` for unset sentence boundaries. This approach can be useful if you want to implement **additional** rules specific to your data, while still being able to take advantage of dependency-based sentence segmentation.

```
import spacy

text = "this is a sentence...hello...and another sentence."

nlp = spacy.load("en_core_web_sm")
doc = nlp(text)
print("Before:", [sent.text for sent in doc.sents])

def set_custom_boundaries(doc):
    for token in doc[:-1]:
        if token.text == "...":
            doc[token.i+1].is_sent_start = True
    return doc

nlp.add_pipe(set_custom_boundaries, before="parser")
doc = nlp(text)
print("After:", [sent.text for sent in doc.sents])
```