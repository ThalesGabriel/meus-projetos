<br/>

The [retokenizer.split](https://spacy.io/api/doc#retokenizer.split) method allows splitting one token into two or more tokens. This can be useful for cases where tokenization rules alone aren’t sufficient. For example, you might want to split “its” into the tokens “it” and “is” — but not the possessive pronoun “its”. You can write rule-based logic that can find only the correct “its” to split, but by that time, the `Doc` will already be tokenized.

<br/>

This process of splitting a token requires more settings, because you need to specify the text of the individual tokens, optional per-token attributes and how the should be attached to the existing syntax tree. This can be done by supplying a list of `heads` – either the token to attach the newly split token to, or a `(token, subtoken)` tuple if the newly split token should be attached to another subtoken. In this case, “New” should be attached to “York” (the second split subtoken) and “York” should be attached to “in”.

```
from spacy import displacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("I live in NewYork")
print("Before:", [token.text for token in doc])
displacy.render(doc)  # displacy.serve if you're not in a Colab environment

with doc.retokenize() as retokenizer:
    heads = [(doc[3], 1), doc[2]]
    attrs = {"POS": ["PROPN", "PROPN"], "DEP": ["pobj", "compound"]}
    retokenizer.split(doc[3], ["New", "York"], heads=heads, attrs=attrs)
print("After:", [token.text for token in doc])
displacy.render(doc)  # displacy.serve if you're not in a Colab environment
```

Specifying the heads as a list of `token` or `(token, subtoken)` tuples allows attaching split subtokens to other subtokens, without having to keep track of the token indices after splitting.

|  TOKEN | HEAD  | DESCRIPTION 
|:---------:|:-----:|:---------:|
|  "New"  | (doc[3], 1) | Attach this token to the second subtoken `(index 1)` that `doc[3]` will be split into, i.e. “York”.
| "York"|  doc[2] | Attach this token to `doc[1]` in the original `Doc`, i.e. “in”. |

If you don’t care about the heads (for example, if you’re only running the tokenizer and not the parser), you can each subtoken to itself:

```
doc = nlp("I live in NewYorkCity")
with doc.retokenize() as retokenizer:
  *->  heads = [(doc[3], 0), (doc[3], 1), (doc[3], 2)]
    retokenizer.split(doc[3], ["New", "York", "City"], heads=heads)
```

> #### Important note
> When splitting tokens, the subtoken texts always have to match the original token text – or, put differently `"".join(subtokens) == token.text` always needs to hold true. If this wasn’t the case, splitting tokens could easily end up producing confusing and unexpected results that would contradict spaCy’s non-destructive tokenization policy.
> ```
>doc = nlp("I live in L.A.")
>with doc.retokenize() as retokenizer:
>-    retokenizer.split(doc[3], ["Los", "Angeles"], heads=[(doc[3], 1), doc[2]])
>+    retokenizer.split(doc[3], ["L.", "A."], heads=[(doc[3], 1), doc[2]])
>```

## Overwriting custom extension attributes

If you’ve registered custom [extension attributes](https://spacy.io/usage/processing-pipelines#custom-components-attributes), you can overwrite them during tokenization by providing a dictionary of attribute names mapped to new values as the `"_"` key in the `attrs`. For merging, you need to provide one dictionary of attributes for the resulting merged token. For splitting, you need to provide a list of dictionaries with custom attributes, one per split subtoken.

> #### Important note
> To set extension attributes during retokenization, the attributes need to be **registered** using the [Token.set_extension](https://spacy.io/api/token#set_extension) method and they need to be **writable**. This means that they should either have a default value that can be overwritten, or a getter and setter. Method extensions or extensions with only a getter are computed dynamically, so their values can’t be overwritten. For more details, see the [extension attribute docs](https://spacy.io/usage/processing-pipelines#custom-components-attributes).

```
import spacy
from spacy.tokens import Token

# Register a custom token attribute, token._.is_musician
Token.set_extension("is_musician", default=False)

nlp = spacy.load("en_core_web_sm")
doc = nlp("I like David Bowie")
print("Before:", [(token.text, token._.is_musician) for token in doc])

with doc.retokenize() as retokenizer:
    retokenizer.merge(doc[2:4], attrs={"_": {"is_musician": True}})
print("After:", [(token.text, token._.is_musician) for token in doc])
```


