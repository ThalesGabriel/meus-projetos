<br/>

spaCy features a fast and accurate syntactic dependency parser, and has a rich API for navigating the tree. The parser also powers the sentence boundary detection, and lets you iterate over base noun phrases, or “chunks”. You can check whether a [Doc](https://spacy.io/api/doc) object has been parsed with the `doc.is_parsed` attribute, which returns a boolean value. If this attribute is **False**, the default sentence iterator will raise an exception.

### Noun Chunks

Noun chunks are "base noun phrases" – flat phrases that have a noun as their head. You can think of noun chunks as a noun plus the words describing the noun – for example, "the lavish green grass" or "the world’s largest tech fund". To get the noun chunks in a document, simply iterate over `Doc.noun_chunks`

P.S. Go to `google colab` and create another block of code, considering that you have already executed the previous code block, you no longer need to import the spacy, nor assign the nlp

```
doc = nlp("Autonomous cars shift insurance liability toward manufacturers")
for chunk in doc.noun_chunks:
    print(chunk.text, chunk.root.text, chunk.root.dep_,
            chunk.root.head.text)
```

|        TEXT        |  ROOT.TEXT   | ROOT.DEP_   | ROOT.HEAD.TEXT |
:--------------------:|:-------------:|:----------:|:--------------:|
|  Autonomous cars    | cars          | nsubj      |     shift      |
| insurance liability | liability     | dobj       |     shift      |
|  manufacturers      | manufacturers | pobj       |     toward     | 

## Navigating the parse tree

spaCy uses the terms **head** and **child** to describe the words **connected by a single arc** in the dependency tree. The term **dep** is used for the arc label, which describes the type of syntactic relation that connects the child to the head. As with other attributes, the value of `.dep` is a hash value. You can get the string value with `.dep_`.

```
doc = nlp("Autonomous cars shift insurance liability toward manufacturers")
for token in doc:
    print(token.text, token.dep_, token.head.text, token.head.pos_,
            [child for child in token.children])
```


|     TEXT    | DEP           | HEAD TEXT   | HEAD POS  | CHILDREN                |
:------------:|:-------------:|:-------------:|:---------:|:-----------------------:|  
|  Autonomous | amod          | cars          |  NOUN     |                         |
|  cars       | nsubj         | shift         |  VERB     | Autonomous              |
|  shift      | ROOT          | shift         |  VERB     | cars, liability, toward |


Because the syntactic relations form a tree, every word has **exactly one head**. You can therefore iterate over the arcs in the tree by iterating over the words in the sentence. This is usually the best way to match an arc of interest — from below:

```
from spacy.symbols import nsubj, VERB

# # Finding a verb with a subject from below — good
verbs = set()
for possible_subject in doc:
    if possible_subject.dep == nsubj and possible_subject.head.pos == VERB:
        verbs.add(possible_subject.head)
print(verbs)
```

If you try to match from above, you’ll have to iterate twice. Once for the head, and then again through the children:

```
# Finding a verb with a subject from above — less good
verbs = []
for possible_verb in doc:
    if possible_verb.pos == VERB:
        for possible_subject in possible_verb.children:
            if possible_subject.dep == nsubj:
                verbs.append(possible_verb)
                break
```

To iterate through the children, use the `token.children` attribute, which provides a sequence of [Token](https://spacy.io/api/token) objects.