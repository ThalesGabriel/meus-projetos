To ground the named entities into the “real world”, spaCy provides functionality to perform entity linking, which resolves a textual entity to a unique identifier from a knowledge base (KB). You can create your own [KnowledgeBase](https://spacy.io/api/kb) and [train a new Entity Linking model](https://spacy.io/usage/training#entity-linker) using that custom-made KB.

## Accessing entity identifiers

The annotated KB identifier is accessible as either a hash value or as a string, using the attributes `ent.kb_id` and `ent.kb_id_` of a [Span](https://spacy.io/api/span) object, or the `ent_kb_id` and `ent_kb_id_` attributes of a [Token](https://spacy.io/api/token) object.

```
nlp = spacy.load("my_custom_el_model")
doc = nlp("Ada Lovelace was born in London")

# document level
ents = [(e.text, e.label_, e.kb_id_) for e in doc.ents]
print(ents)  # [('Ada Lovelace', 'PERSON', 'Q7259'), ('London', 'GPE', 'Q84')]

# token level
ent_ada_0 = [doc[0].text, doc[0].ent_type_, doc[0].ent_kb_id_]
ent_ada_1 = [doc[1].text, doc[1].ent_type_, doc[1].ent_kb_id_]
ent_london_5 = [doc[5].text, doc[5].ent_type_, doc[5].ent_kb_id_]
print(ent_ada_0)  # ['Ada', 'PERSON', 'Q7259']
print(ent_ada_1)  # ['Lovelace', 'PERSON', 'Q7259']
print(ent_london_5)  # ['London', 'GPE', 'Q84']
```

|    TEXT        | ENT_TYPE_  | ENT_KB_ID_   |
:----------------:|:----:|:-----:|
|  Ada  | "PERSON" | "Q7259" |
|  Lovelace    | "PERSON" | "Q7259"   | 
|  was     | - | -  |
|  born     | -  | -  |
|  in     | -  | -  | 
|  London	    | "GPE" | "Q84"  | 

## Tokenization

Tokenization is the task of splitting a text into meaningful segments, called tokens. The input to the tokenizer is a unicode text, and the output is a [Doc](https://spacy.io/api/doc) object. To construct a `Doc` object, you need a [Vocab](https://spacy.io/api/vocab) instance, a sequence of `word` strings, and optionally a sequence of `spaces` booleans, which allow you to maintain alignment of the tokens into the original string.

> #### Important note
> spaCy’s tokenization is **non-destructive**, which means that you’ll always be able to reconstruct the original input from the tokenized output. Whitespace information is preserved in the tokens and no information is added or removed during tokenization. This is kind of a core principle of spaCy’s `Doc` object: `doc.text == input_text` should always hold true.

During processing, spaCy first **tokenizes** the text, i.e. segments it into words, punctuation and so on. This is done by applying rules specific to each language. For example, punctuation at the end of a sentence should be split off – whereas “U.K.” should remain one token. Each Doc consists of individual tokens, and we can iterate over them:

```

nlp = spacy.load("en_core_web_sm")
doc = nlp("Apple is looking at buying U.K. startup for $1 billion")
for token in doc:
    print(token.text)
```

|  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  10  |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:----:|
|  Apple  | is | looking | at | buying | U.K. | Startup | for | $ | 1 | billion

First, the raw text is split on whitespace characters, similar to `text.split(' ')`. Then, the tokenizer processes the text from left to right. On each substring, it performs two checks:

1. **Does the substring match a tokenizer exception rule?** For example, “don’t” does not contain whitespace, but should be split into two tokens, “do” and “n’t”, while “U.K.” should always remain one token.

2. **Can a prefix, suffix or infix be split off?** For example punctuation like commas, periods, hyphens or quotes.

If there’s a match, the rule is applied and the tokenizer continues its loop, starting with the newly split substrings. This way, spaCy can split **complex, nested tokens** like combinations of abbreviations and multiple punctuation marks.


While punctuation rules are usually pretty general, tokenizer exceptions strongly depend on the specifics of the individual language. This is why each [available language](https://spacy.io/usage/models#languages) has its own subclass like `English` or `German`, that loads in lists of hard-coded data and exception rules.