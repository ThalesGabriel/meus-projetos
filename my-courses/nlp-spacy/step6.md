<br/>

The standard way to access entity annotations is the [doc.ents](https://spacy.io/api/doc#ents) property, which produces a sequence of [Span](https://spacy.io/api/span) objects. The entity type is accessible either as a hash value or as a string, using the attributes `ent.label` and `ent.label_`. The Span object acts as a sequence of tokens, so you can iterate over the entity or index into it. You can also get the text form of the whole entity, as though it were a single token.

<br/>

You can also access token entity annotations using the [token.ent_iob](https://spacy.io/api/token#attributes) and [token.ent_type](https://spacy.io/api/token#attributes) attributes. `token.ent_iob` indicates whether an entity starts, continues or ends on the tag. If no entity type is set on a token, it will return an empty string.

```
doc = nlp("San Francisco considers banning sidewalk delivery robots")

# document level
ents = [(e.text, e.start_char, e.end_char, e.label_) for e in doc.ents]
print(ents)

# token level
ent_san = [doc[0].text, doc[0].ent_iob_, doc[0].ent_type_]
ent_francisco = [doc[1].text, doc[1].ent_iob_, doc[1].ent_type_]
print(ent_san)  # ['San', 'B', 'GPE']
print(ent_francisco)  # ['Francisco', 'I', 'GPE']
```

|    TEXT                              | ENT_IOB  | ENT_IOB_   | ENT_TYPE_ | DESCRIPTION
:--------------------------------------:|:----:|:-----:|:---------:|:---------:|
|  San  | 3 | B |  "GPE"   | beginning of an entity
|  Francisco    | 1 | I   |  "GPE"  | inside an entity
|  considers     | 2 | 0  |  ""   |outside an entity
|  banning     | 2  | 0  |  "" | outside an entity
|  sidewalk    | 2 | 0  |  ""   | outside an entity

## Setting entity annotations

To ensure that the sequence of token annotations remains consistent, you have to set entity annotations **at the document level**. However, you can’t write directly to the `token.ent_iob` or `token.ent_type` attributes, so the easiest way to set entities is to assign to the [doc.ents](https://spacy.io/api/doc#ents) attribute and create the new entity as a [Span](https://spacy.io/api/span)

```
from spacy.tokens import Span

doc = nlp("fb is hiring a new vice president of global policy")
ents = [(e.text, e.start_char, e.end_char, e.label_) for e in doc.ents]
print('Before', ents)
# the model didn't recognise "fb" as an entity :(

fb_ent = Span(doc, 0, 1, label="ORG") # create a Span for the new entity
doc.ents = list(doc.ents) + [fb_ent]

ents = [(e.text, e.start_char, e.end_char, e.label_) for e in doc.ents]
print('After', ents)
# [('fb', 0, 2, 'ORG')] 🎉
```

Keep in mind that you need to create a `Span` with the start and end index of the **token**, not the start and end index of the entity in the document. In this case, "fb" is token `(0, 1)` – but at the document level, the entity will have the start and end indices `(0, 2)`.

## Setting entity annotations from array

You can also assign entity annotations using the [doc.from_array](https://spacy.io/api/doc#from_array) method. To do this, you should include both the `ENT_TYPE` and the `ENT_IOB `attributes in the array you’re importing from.

```
import numpy
from spacy.attrs import ENT_IOB, ENT_TYPE

doc = nlp.make_doc("London is a big city in the United Kingdom.")
print("Before", doc.ents)  # []

header = [ENT_IOB, ENT_TYPE]
attr_array = numpy.zeros((len(doc), len(header)), dtype="uint64")
attr_array[0, 0] = 3  # B
attr_array[0, 1] = doc.vocab.strings["GPE"]
doc.from_array(header, attr_array)
print("After", doc.ents)  # [London]
```