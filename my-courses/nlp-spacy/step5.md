<br/>

The best way to understand spaCy’s dependency parser is interactively. To make this easier, spaCy v2.0+ comes with a visualization module. You can pass a `Doc` or a list of `Doc` objectsto displaCy and run [displacy.serve](https://spacy.io/api/top-level#displacy.serve) to run the web server, or [displacy.render](https://spacy.io/api/top-level#displacy.render) to generate the raw markup. If you want to know how to write rules that hook into some type of syntactic construction, just plug the sentence into the visualizer and see how spaCy annotates it.
```
from spacy import displacy
doc = nlp("Autonomous cars shift insurance liability toward manufacturers")
# Since this is an interactive `google colab` environment, we can use displacy.render here
displacy.render(doc, style='dep')
```

> For more details and examples, see the [usage guide on visualizing spaCy](https://spacy.io/usage/visualizers). You can also test displaCy in [demo](https://explosion.ai/demos/displacy).

## Disabling the parser

In the [default models](https://spacy.io/models), the parser is loaded and enabled as part of the [standard processing pipeline](https://spacy.io/usage/processing-pipelines). If you don’t need any of the syntactic information, you should disable the parser. Disabling the parser will make spaCy load and run much faster. If you want to load the parser, but need to disable it for specific documents, you can also control its use on the `nlp` object.

```
nlp = spacy.load("en_core_web_sm", disable=["parser"])
nlp = English().from_disk("/model", disable=["parser"])
doc = nlp("I don't want parsed", disable=["parser"])
```

> #### Important note: disabling pipeline components
> Since spaCy v2.0 comes with better support for customizing the processing pipeline components, the `parser`, keyword argument has been replaced with `disable`, which takes a list of [pipeline component names](https://spacy.io/usage/processing-pipelines). This lets you disable both default and custom components when loading a model, or initializing a Language class via [from_disk](https://spacy.io/api/language#from_disk).

```
+ nlp = spacy.load("en_core_web_sm", disable["parser"])
+ doc = nlp("I don't want parsed", disable=["parser"])

- nlp = spacy.load("en_core_web_sm", parser=False)
- doc = nlp("I don't want parsed", parse=False)
``` 

## Named Entity Recognition

<br/>

spaCy features an extremely fast statistical entity recognition system, that assigns labels to contiguous spans of tokens. The default model identifies a variety of named and numeric entities, including companies, locations, organizations and products. You can add arbitrary classes to the entity recognition system, and update the model with new examples.

### Named Entity Recognition 101

A named entity is a “real-world object” that’s assigned a name – for example, a person, a country, a product or a book title. spaCy can **recognize** [various types](https://spacy.io/api/annotation#named-entities) of named entities in a document, by asking the model for a **prediction**. Because models are statistical and strongly depend on the examples they were trained on, this doesn’t always work perfectly and might need some tuning later, depending on your use case.

<br/>

Named entities are available as the `ents` property of a `Doc`:

```

doc = nlp("Apple is looking at buying U.K. startup for $1 billion")

for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)
```

|    TEXT                              | START  | END   | LABEL | DESCRIPTION | 
:--------------------------------------:|:----:|:-----:|:---------:|:---------:|
|  Apple  | 0 | 5 |  ORG   | Companies, agencies, institutions |
|  U.K.                                 | VERB | 27   |  31   | GPE |
|  $1 billion                               | 44 | 54  |  MONEY   | Monetary values, including unit.

Using spaCy’s built-in [displaCy visualizer](https://spacy.io/usage/visualizers), here’s what our example sentence and its named entities look like: