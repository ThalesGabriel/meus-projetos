<br/>

Finally, you can always write to the underlying struct, if you compile a [Cython](http://cython.org/) function. This is easy to do, and allows you to write efficient native code.

```
# cython: infer_types=True
from spacy.tokens.doc cimport Doc

cpdef set_entity(Doc doc, int start, int end, int ent_type):
    for i in range(start, end):
        doc.c[i].ent_type = ent_type
    doc.c[start].ent_iob = 3
    for i in range(start+1, end):
        doc.c[i].ent_iob = 2
```

Obviously, if you write directly to the array of TokenC* structs, you’ll have responsibility for ensuring that the data is left in a consistent state.

## Built-in entity types

> #### Annotation scheme
> For details on the entity types available in spaCy’s pretrained models, see the [NER annotation scheme](https://spacy.io/api/annotation#named-entities).

## Training and updating

To provide training examples to the entity recognizer, you’ll first need to create an instance of the [GoldParse](https://spacy.io/api/goldparse) class. You can specify your annotations in a stand-off format or as token tags. If a character offset in your entity annotations doesn’t fall on a token boundary, the `GoldParse` class will treat that annotation as a missing value. This allows for more realistic training, because the entity recognizer is allowed to learn from examples that may feature tokenizer errors.

```
train_data = [
    ("Who is Chaka Khan?", [(7, 17, "PERSON")]),
    ("I like London and Berlin.", [(7, 13, "LOC"), (18, 24, "LOC")]),
]
```

```
doc = Doc(nlp.vocab, ["rats", "make", "good", "pets"])
gold = GoldParse(doc, entities=["U-ANIMAL", "O", "O", "O"])
```

> For more details on **training and updating** the named entity recognizer, see the usage guides on [training](https://spacy.io/usage/training) or check out the runnable [training script](https://github.com/explosion/spaCy/blob/master/examples/training/train_ner.py) on GitHub.

## Visualizing named entities

The [displaCy ENT visualizer](https://explosion.ai/demos/displacy-ent) lets you explore an entity recognition model’s behavior interactively. If you’re training a model, it’s very useful to run the visualization yourself. To help you do that, spaCy v2.0+ comes with a visualization module. You can pass a Doc or a list of Doc objects to displaCy and run [displacy.serve](https://spacy.io/api/top-level#displacy.serve) to run the web server, or [displacy.render](https://spacy.io/api/top-level#displacy.render) to generate the raw markup.

<br/>

For more details and examples, see the [usage guide on visualizing spaCy](https://spacy.io/usage/visualizers)


```
from spacy import displacy

text = "When Sebastian Thrun started working on self-driving cars at Google in 2007, few people outside of the company took him seriously."

nlp = spacy.load("en_core_web_sm")
doc = nlp(text)
displacy.serve(doc, style="ent")
```