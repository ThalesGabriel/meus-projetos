<br/>

The tokenizer is the first component of the processing pipeline and the only one that can’t be replaced by writing to `nlp.pipeline`. This is because it has a different signature from all the other components: it takes a text and returns a `Doc`, whereas all other components expect to already receive a tokenized `Doc`.

To overwrite the existing tokenizer, you need to replace nlp.tokenizer with a custom function that takes a text, and returns a `Doc`.

```
nlp = spacy.load("en_core_web_sm")
nlp.tokenizer = my_tokenizer
```

|  ARGUMENT | TYPE  | DESCRIPTION 
|:---------:|:-----:|:---------:|
|  text  | unicode | The raw text to tokenize.
| RETURNS| Document | The tokenized document.   |

> #### Important note: using a custom tokenizer
> In spaCy v1.x, you had to add a custom tokenizer by passing it to the `make_doc` keyword argument, or by passing a tokenizer “factory” to `create_make_doc`. This was unnecessarily complicated. Since spaCy v2.0, you can write to `nlp.tokenizer` instead. If your tokenizer needs the vocab, you can write a function and use `nlp.vocab`.
> ```
> - nlp = spacy.load("en_core_web_sm", > make_doc=my_tokenizer)
> - nlp = spacy.load("en_core_web_sm", > create_make_doc=my_tokenizer_factory)
> 
> + nlp.tokenizer = my_tokenizer
> + nlp.tokenizer = my_tokenizer_factory(nlp.vocab)
> ```

## Example: A custom whitespace tokenizer

To construct the tokenizer, we usually want attributes of the nlp pipeline. Specifically, we want the tokenizer to hold a reference to the vocabulary object. Let’s say we have the following class as our tokenizer:

```
from spacy.tokens import Doc

class WhitespaceTokenizer(object):
    def __init__(self, vocab):
        self.vocab = vocab

    def __call__(self, text):
        words = text.split(' ')
        # All tokens 'own' a subsequent space character in this tokenizer
        spaces = [True] * len(words)
        return Doc(self.vocab, words=words, spaces=spaces)

nlp = spacy.load("en_core_web_sm")
nlp.tokenizer = WhitespaceTokenizer(nlp.vocab)
doc = nlp("What's happened to me? he thought. It wasn't a dream.")
print([t.text for t in doc])
```

As you can see, we need a `Vocab` instance to construct this — but we won’t have it until we get back the loaded `nlp` object. The simplest solution is to build the tokenizer in two steps. This also means that you can reuse the “tokenizer factory” and initialize it with different instances of `Vocab`.

## Bringing your own annotations

spaCy generally assumes by default that your data is raw text. However, sometimes your data is partially annotated, e.g. with pre-existing tokenization, part-of-speech tags, etc. The most common situation is that you have pre-defined tokenization. If you have a list of strings, you can create a `Doc` object directly. Optionally, you can also specify a list of boolean values, indicating whether each word has a subsequent space.

```
from spacy.tokens import Doc
from spacy.lang.en import English

nlp = English()
doc = Doc(nlp.vocab, words=["Hello", ",", "world", "!"],
          spaces=[False, True, False, False])
print([(t.text, t.text_with_ws, t.whitespace_) for t in doc])
```

If provided, the spaces list must be the same length as the words list. The spaces list affects the `doc.text`, `span.text`, `token.idx`, s`pan.start_char` and `span.end_char` attributes. If you don’t provide a `spaces` sequence, spaCy will assume that all words are whitespace delimited.

```
from spacy.tokens import Doc
from spacy.lang.en import English

nlp = English()
bad_spaces = Doc(nlp.vocab, words=["Hello", ",", "world", "!"])
good_spaces = Doc(nlp.vocab, words=["Hello", ",", "world", "!"],
                  spaces=[False, True, False, False])

print(bad_spaces.text)   # 'Hello , world !'
print(good_spaces.text)  # 'Hello, world!'
```

Once you have a [Doc](https://spacy.io/api/doc) object, you can write to its attributes to set the part-of-speech tags, syntactic dependencies, named entities and other attributes. For details, see the respective usage pages.

