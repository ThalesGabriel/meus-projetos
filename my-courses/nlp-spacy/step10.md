<br/>

A working implementation of the pseudo-code above is available for debugging as [nlp.tokenizer.explain(text)](https://spacy.io/usage/linguistic-features#tokenizer-debug). It returns a list of tuples showing which tokenizer rule or pattern was matched for each token. The tokens produced are identical to `nlp.tokenizer()` except for whitespace tokens:

```
from spacy.lang.en import English

nlp = English()
text = '''"Let's go!"'''
doc = nlp(text)
tok_exp = nlp.tokenizer.explain(text)
assert [t.text for t in doc if not t.is_space] == [t[1] for t in tok_exp]
for t in tok_exp:
    print(t[1], "\t", t[0])

# "      PREFIX
# Let    SPECIAL-1
# 's     SPECIAL-2
# go     TOKEN
# !      SUFFIX
# "      SUFFIX
```

## Customizing spaCy’s Tokenizer class

Let’s imagine you wanted to create a tokenizer for a new language or specific domain. There are six things you may need to define:

1. A dictionary of **special cases**. This handles things like contractions, units of measurement, emoticons, certain abbreviations, etc.
2. A function `prefix_search`, to handle **preceding punctuation**, such as open quotes, open brackets, etc.
3. A function `suffix_search`, to handle **succeeding punctuation**, such as commas, periods, close quotes, etc.
4. A function `infixes_finditer`, to handle non-whitespace separators, such as hyphens etc.
5. An optional boolean function `token_match` matching strings that should never be split, overriding the infix rules. Useful for things like numbers.
6. An optional boolean function `url_match`, which is similar to `token_match` except that prefixes and suffixes are removed before applying the match.

> #### Important note: token match in spaCy v2.2
> In spaCy v2.2.2-v2.2.4, the `token_match` was equivalent to the `url_match` above and there was no match pattern applied before prefixes and suffixes were analyzed. As of spaCy v2.3.0, the `token_match` has been reverted to its behavior in v2.2.1 and earlier with precedence over prefixes and suffixes.<br/><br/>
> The `url_match` is introduced in v2.3.0 to handle cases like URLs where the tokenizer should remove prefixes and suffixes (e.g., a comma at the end of a URL) before applying the match.

You shouldn’t usually need to create a `Tokenizer` subclass. Standard usage is to use `re.compile()` to build a regular expression object, and pass its `.search()` and `.finditer()` methods:

```
import re
from spacy.tokenizer import Tokenizer

special_cases = {":)": [{"ORTH": ":)"}]}
prefix_re = re.compile(r'''^[[("']''')
suffix_re = re.compile(r'''[])"']$''')
infix_re = re.compile(r'''[-~]''')
simple_url_re = re.compile(r'''^https?://''')

def custom_tokenizer(nlp):
    return Tokenizer(nlp.vocab, rules=special_cases,
                                prefix_search=prefix_re.search,
                                suffix_search=suffix_re.search,
                                infix_finditer=infix_re.finditer,
                                url_match=simple_url_re.match)

nlp = spacy.load("en_core_web_sm")
nlp.tokenizer = custom_tokenizer(nlp)
doc = nlp("hello-world. :)")
print([t.text for t in doc]) # ['hello', '-', 'world.', ':)']
```

If you need to subclass the tokenizer instead, the relevant methods to specialize are `find_prefix`, `find_suffix` and `find_infix`.

> #### Important note
> When customizing the prefix, suffix and infix handling, remember that you’re passing in **functions** for spaCy to execute, e.g. `prefix_re.search` – not just the regular expressions. This means that your functions also need to define how the rules should be applied. For example, if you’re adding your own prefix rules, you need to make sure they’re only applied to characters at the **beginning of a token**, e.g. by adding `^`. Similarly, suffix rules should only be applied at the **end of a token**, so your expression should end with a `$`.