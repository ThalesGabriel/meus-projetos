<br/>

In `NLTK` it is necessary that we do the` tokenization` of the text so that we can make the processor of `PoS Tagging`, in` SpaCy` it is all one thing.

<br/>

Now I will ask you to go to [google colab] (https://colab.research.google.com/notebooks/intro.ipynb#recent=true) and create your first notebook.Paste the code snippet below

```
import spacy

nlp = spacy.load("en_core_web_sm")

doc = nlp("Apple is looking at buying U.K. startup for $1 billion")

for token in doc:
  print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_, token.shape_, token.is_alpha, token.is_stop)

```

This is where the statistical model comes in, which allows `SpaCy` to predict which tag is likely to apply in this context.

<br/>

After copying and pasting in `Colab`, execute the code block and at the end of this step we will have something like this table:

|  Texto    | Lemma | PoS   |  Tag  |  DEP  | Forma | Alpha | Stopword |
:----------:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:--------:|
|  Apple    | apple | PROPN | NNP   | nsubj | Xxxxx | True  | False    |
|  is       | be    | AUX   | VBZ   | aux   | xx    | True  | True     |
|  looking  | look  | VERB  | VBG   | ROOT  | xxxx  | True  | False    |

<br/>

P.S. Throughout this course tables will be common to show you the code output

## Rule-based morphology


Inflectional morphology is the process by which a root form of a word is modified by adding prefixes or suffixes that specify its grammatical function but do not changes its part-of-speech. 

<br/>

We say that a `lemma`(root form) is `inflected` (modified/combined) with one or more `morphological features` to create a surface form. Here are some examples:

|                 Context                 | SURFACE | Lemma   | PoS   |     Morphological Features  | 
:-----------------------------------------:|:-----------:|:------:|:-----:|:------------:|
|                  I was reading the paper | reading    | read    | verb  | VerbForm=Ger |
| I don’t watch the news, I read the paper | read       | read    | verb  | VerbForm=Fin, Mood=Ind, Tense=Pres |
|               I read the paper yesterday | read       | read    | verb  | VerbForm=Fin, Mood=Ind, Tense=Past |

This is where the statistical model comes in, which allows `SpaCy` to predict which tag or label is likely to apply in this context.

<br/>

English has a relatively simple morphological system, which spaCy handles using rules that can be keyed by the token, the part-of-speech tag, or the combination of the two. The system works as follows:

<br/>

1. The tokenizer consults a [mapping table](https://spacy.io/usage/adding-languages#tokenizer-exceptions) `TOKENIZER_EXCEPTIONS`, which allows sequences of characters to be mapped to multiple tokens. Each token may be assigned a part of speech and one or more morphological features.

2. The part-of-speech tagger then assigns each token an **extended POS tag**. In the API, these tags are known as Token.tag. They express the part-of-speech (e.g. VERB) and some amount of morphological information, e.g. that the verb is past tense.

3. For words whose POS is not set by a prior process, a [mapping table](https://spacy.io/usage/adding-languages#tokenizer-exceptions) `TAG_MAP` maps the tags to a part-of-speech and a set of morphological features.

4. Finally, a **rule-based deterministic lemmatizer** maps the surface form, to a lemma in light of the previously assigned extended part-of-speech and morphological information, without consulting the context of the token. The lemmatizer also accepts list-based exception files, acquired from [Wordnet](https://wordnet.princeton.edu/)