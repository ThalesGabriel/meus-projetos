<br/>

A few more convenience attributes are provided for iterating around the local tree from the token. `Token.lefts` and `Token.rights` attributes provide sequences of syntactic children that occur before and after the token. Both sequences are in sentence order. There are also two integer-typed attributes, `Token.n_lefts` and `Token.n_rights` that give the number of left and right children.

```
doc = nlp("bright red apples on the tree")
print([token.text for token in doc[2].lefts])  # ['bright', 'red']
print([token.text for token in doc[2].rights])  # ['on']
print(doc[2].n_lefts)  # 2
print(doc[2].n_rights) 
```

```

nlp_de = spacy.load("de_core_news_sm")
doc_de = nlp_de("schöne rote Äpfel auf dem Baum")
print([token.text for token in doc_de[2].lefts])  # ['schöne', 'rote']
print([token.text for token in doc_de[2].rights])  # ['auf']
```

You can get a whole phrase by its syntactic head using the `Token.subtree` attribute. This returns an ordered sequence of tokens. You can walk up the tree with the `Token.ancestors` attribute, and check dominance with `Token.is_ancestor`.

```
doc = nlp("Credit and mortgage account holders must submit their requests")
​
root = [token for token in doc if token.head == token][0]
subject = list(root.lefts)[0]
for descendant in subject.subtree:
    assert subject is descendant or subject.is_ancestor(descendant)
    print(descendant.text, descendant.dep_, descendant.n_lefts,
            descendant.n_rights,
            [ancestor.text for ancestor in descendant.ancestors])
```

|    TEXT   | DEP       | N_LEFTS | N_RIGHTS  | ANCESTORS                        |
:-----------:|:---------:|:-------:|:---------:|:--------------------------------:|  
|  Credit    | nmod      | 0       |  2        | holders, submit                  |
|  and       | cc        | 0       |  0        | holders, submit                  |
|  mortgage  | compound  | 0       |  0        | account, credit, holders, submit |
|  account   | compound  | 1       |  0        | credit, holders, submit          |
|  holders   | compound  | 1       |  0        | submit                           |

Finally, the `.left_edge` and `.right_edge` attributes can be especially useful, because they give you the first and last token of the subtree. This is the easiest way to create a `Span` object for a syntactic phrase. Note that `.right_edge` gives a token **within** the subtree — so if you use it as the end-point of a range, don’t forget to `+1`!

```
doc = nlp("Credit and mortgage account holders must submit their requests")
span = doc[doc[4].left_edge.i : doc[4].right_edge.i+1]
with doc.retokenize() as retokenizer:
    retokenizer.merge(span)
for token in doc:
    print(token.text, token.pos_, token.dep_, token.head.text)
```

|    TEXT                              | POS  | DEP   | HEAD TEXT | 
:--------------------------------------:|:----:|:-----:|:---------:|
|  Credit and mortgage account holders  | NOUN | nsubj |  submit   |
|  must                                 | VERB | aux   |  submit   | 
|  submit                               | VERB | ROOT  |  submit   |
|  their                                | ADJ  | poss  |  requests | 
|  requests                             | NOUN | dobj  |  submit   | 


> #### Dependency label scheme
> For a list of the syntactic dependency labels assigned by spaCy’s models across different languages, see the [dependency label scheme documentation](https://spacy.io/api/annotation#dependency-parsing).