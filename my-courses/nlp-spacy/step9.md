<br/>

**Global** and **language-specific** tokenizer data is supplied via the language data in [spacy/lang](https://github.com/explosion/spaCy/tree/master/spacy/lang). The tokenizer exceptions define special cases like “don’t” in English, which needs to be split into two tokens: `{ORTH: "do"}` and `{ORTH: "n't", NORM: "not"}`. The prefixes, suffixes and infixes mostly define punctuation rules – for example, when to split off periods (at the end of a sentence), and when to leave tokens containing periods intact (abbreviations like “U.S.”).

> #### 📖 Language data
> For more details on the language-specific data, see the usage guide on [adding languages](https://spacy.io/usage/adding-languages).

1. **Should I change the language data or add custom tokenizer rules?** Tokenization rules that are specific to one language, but can be **generalized across that language** should ideally live in the language data in [spacy/lang](https://github.com/explosion/spaCy/tree/master/spacy/lang) – we always appreciate pull requests! Anything that’s specific to a domain or text type – like financial trading abbreviations, or Bavarian youth slang – should be added as a special case rule to your tokenizer instance. If you’re dealing with a lot of customizations, it might make sense to create an entirely custom subclass.

## Adding special case tokenization rules

Most domains have at least some idiosyncrasies that require custom tokenization rules. This could be very certain expressions, or abbreviations only used in this specific field. Here’s how to add a special case rule to an existing [Tokenizer](https://spacy.io/api/tokenizer) instance:

```
from spacy.symbols import ORTH

nlp = spacy.load("en_core_web_sm")
doc = nlp("gimme that")  # phrase to tokenize
print([w.text for w in doc])  # ['gimme', 'that']

# Add special case rule
special_case = [{ORTH: "gim"}, {ORTH: "me"}]
nlp.tokenizer.add_special_case("gimme", special_case)

# Check new tokenization
print([w.text for w in nlp("gimme that")])  # ['gim', 'me', 'that']
```

The special case doesn’t have to match an entire whitespace-delimited substring. The tokenizer will incrementally split off punctuation, and keep looking up the remaining substring:

```
assert "gimme" not in [w.text for w in nlp("gimme!")]
assert "gimme" not in [w.text for w in nlp('("...gimme...?")')]
```

The special case rules have precedence over the punctuation splitting:

```
nlp.tokenizer.add_special_case("...gimme...?", [{"ORTH": "...gimme...?"}])
assert len(nlp("...gimme...?")) == 1
```

## How spaCy’s tokenizer works

spaCy introduces a novel tokenization algorithm, that gives a better balance between performance, ease of definition, and ease of alignment into the original string.

After consuming a prefix or suffix, we consult the special cases again. We want the special cases to handle things like “don’t” in English, and we want the same rule to work for “(don’t)!“. We do this by splitting off the open bracket, then the exclamation, then the close bracket, and finally matching the special case. Here’s an implementation of the algorithm in Python, optimized for readability rather than performance:

```
def tokenizer_pseudo_code(self, special_cases, prefix_search, suffix_search,
                          infix_finditer, token_match, url_match):
    tokens = []
    for substring in text.split():
        suffixes = []
        while substring:
            while prefix_search(substring) or suffix_search(substring):
                if token_match(substring):
                    tokens.append(substring)
                    substring = ''
                    break
                if substring in special_cases:
                    tokens.extend(special_cases[substring])
                    substring = ''
                    break
                if prefix_search(substring):
                    split = prefix_search(substring).end()
                    tokens.append(substring[:split])
                    substring = substring[split:]
                    if substring in special_cases:
                        continue
                if suffix_search(substring):
                    split = suffix_search(substring).start()
                    suffixes.append(substring[split:])
                    substring = substring[:split]
            if token_match(substring):
                tokens.append(substring)
                substring = ''
            elif url_match(substring):
                tokens.append(substring)
                substring = ''
            elif substring in special_cases:
                tokens.extend(special_cases[substring])
                substring = ''
            elif list(infix_finditer(substring)):
                infixes = infix_finditer(substring)
                offset = 0
                for match in infixes:
                    tokens.append(substring[offset : match.start()])
                    tokens.append(substring[match.start() : match.end()])
                    offset = match.end()
                if substring[offset:]:
                    tokens.append(substring[offset:])
                substring = ''
            elif substring:
                tokens.append(substring)
                substring = ''
        tokens.extend(reversed(suffixes))
    return tokens
```

The algorithm can be summarized as follows:

1. Iterate over whitespace-separated substrings.
2. Look for a token match. If there is a match, stop processing and keep this token.
3. Check whether we have an explicitly defined special case for this substring. If we do, use it.
4. Otherwise, try to consume one prefix. If we consumed a prefix, go back to #2, so that the token match and special cases always get priority.
5. If we didn’t consume a prefix, try to consume a suffix and then go back to #2.
6. If we can’t consume a prefix or a suffix, look for a URL match.
7. If there’s no URL match, then look for a special case.
8. Look for “infixes” — stuff like hyphens etc. and split the substring into tokens on all infixes.
9. Once we can’t consume any more of the string, handle it as a single token.