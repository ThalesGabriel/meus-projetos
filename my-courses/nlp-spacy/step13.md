<br/>

spaCy’s tokenization is non-destructive and uses language-specific rules optimized for compatibility with treebank annotations. Other tools and resources can sometimes tokenize things differently – for example, `"I'm" → ["I", "'", "m"]` instead of `["I", "'m"]`.

<br/>

In situations like that, you often want to align the tokenization so that you can merge annotations from different sources together, or take vectors predicted by a [pretrained BERT model](https://github.com/huggingface/transformers) and apply them to spaCy tokens. spaCy’s [gold.align](https://spacy.io/api/goldparse#align) helper returns a `(cost, a2b, b2a, a2b_multi, b2a_multi)` tuple describing the number of misaligned tokens, the one-to-one mappings of token indices in both directions and the indices where multiple tokens align to one single token.

```
from spacy.gold import align

other_tokens = ["i", "listened", "to", "obama", "'", "s", "podcasts", "."]
spacy_tokens = ["i", "listened", "to", "obama", "'s", "podcasts", "."]
cost, a2b, b2a, a2b_multi, b2a_multi = align(other_tokens, spacy_tokens)
print("Edit distance:", cost)  # 3
print("One-to-one mappings a -> b", a2b)  # array([0, 1, 2, 3, -1, -1, 5, 6])
print("One-to-one mappings b -> a", b2a)  # array([0, 1, 2, 3, -1, 6, 7])
print("Many-to-one mappings a -> b", a2b_multi)  # {4: 4, 5: 4}
print("Many-to-one mappings b-> a", b2a_multi)  # {}
```

Here are some insights from the alignment information generated in the example above:

* The edit distance (cost) is 3: two deletions and one insertion.
* The one-to-one mappings for the first four tokens are identical, which means they map to each other. This makes sense because they’re also identical in the input: "i", "listened", "to" and "obama".
* The index mapped to a2b[6] is 5, which means that other_tokens[6] ("podcasts") aligns to spacy_tokens[5] (also "podcasts").
* a2b[4] is -1, which means that there is no one-to-one alignment for the token at other_tokens[4]. The token "'" doesn’t exist on its own in spacy_tokens. The same goes for a2b[5] and other_tokens[5], i.e. "s".
* The dictionary a2b_multi shows that both tokens 4 and 5 of other_tokens ("'" and "s") align to token 4 of spacy_tokens ("'s").
* The dictionary b2a_multi shows that there are no tokens in spacy_tokens that map to multiple tokens in other_tokens.

> #### Important note
> The current implementation of the alignment algorithm assumes that both tokenizations add up to the same string. For example, you’ll be able to align `["I", "'", "m"]` and `["I", "'m"]`, which both add up to `"I'm"`, but not `["I", "'m"]` and `["I", "am"]`.

## Merging and splitting V2.1

The [Doc.retokenize](https://spacy.io/api/doc#retokenize) context manager lets you merge and split tokens. Modifications to the tokenization are stored and performed all at once when the context manager exits. To merge several tokens into one single token, pass a `Span` to [retokenizer.merge](https://spacy.io/api/doc#retokenizer.merge). An optional dictionary of `attrs` lets you set attributes that will be assigned to the merged token – for example, the lemma, part-of-speech tag or entity type. By default, the merged token will receive the same attributes as the merged span’s root.

```
import spacy

nlp = spacy.load("en_core_web_sm")
doc = nlp("I live in New York")
print("Before:", [token.text for token in doc])

with doc.retokenize() as retokenizer:
    retokenizer.merge(doc[3:5], attrs={"LEMMA": "new york"})
print("After:", [token.text for token in doc])
```

If an attribute in the attrs is a context-dependent token attribute, it will be applied to the underlying [Token](https://spacy.io/api/token). For example `LEMMA`, `POS` or `DEP` only apply to a word in context, so they’re token attributes. If an attribute is a context-independent lexical attribute, it will be applied to the underlying [Lexeme](https://spacy.io/api/lexeme), the entry in the vocabulary. For example, `LOWER` or `IS_STOP` apply to all words of the same spelling, regardless of the context.

> #### Note on merging overlapping spans
> If you’re trying to merge spans that overlap, spaCy will raise an error because it’s unclear how the result should look. Depending on the application, you may want to match the shortest or longest possible span, so it’s up to you to filter them. If you’re looking for the longest non-overlapping span, you can use the [util.filter_spans](https://spacy.io/api/top-level#util.filter_spans) helper:
> ```
> doc = nlp("I live in Berlin Kreuzberg")
> spans = [doc[3:5], doc[3:4], doc[4:5]]
> filtered_spans = filter_spans(spans)
> ```

