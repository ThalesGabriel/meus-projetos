<br/>

In many situations, you don’t necessarily need entirely custom rules. Sometimes you just want to add another character to the prefixes, suffixes or infixes. The default prefix, suffix and infix rules are available via the `nlp` object’s `Defaults` and the `Tokenizer` attributes such as [Tokenizer.suffix_search](https://spacy.io/api/tokenizer#attributes) are writable, so you can overwrite them with compiled regular expression objects using modified default rules. spaCy ships with utility functions to help you compile the regular expressions – for example, [compile_suffix_regex](https://spacy.io/api/top-level#util.compile_suffix_regex):

```
suffixes = nlp.Defaults.suffixes + (r'''-+$''',)
suffix_regex = spacy.util.compile_suffix_regex(suffixes)
nlp.tokenizer.suffix_search = suffix_regex.search
```

Similarly, you can remove a character from the default suffixes:

```
suffixes = list(nlp.Defaults.suffixes)
suffixes.remove("\\[")
suffix_regex = spacy.util.compile_suffix_regex(suffixes)
nlp.tokenizer.suffix_search = suffix_regex.search
```

The `Tokenizer.suffix_search` attribute should be a function which takes a unicode string and returns a **regex match object** or `None`. Usually we use the `.search` attribute of a compiled regex object, but you can use some other function that behaves the same way.


> #### Important note
> If you’re using a statistical model, writing to the `nlp.Defaults` or `English.Defaults` directly won’t work, since the regular expressions are read from the model and will be compiled when you load it. If you modify `nlp.Defaults`, you’ll only see the effect if you call [spacy.blank](https://spacy.io/api/top-level#spacy.blank) or `Defaults.create_tokenizer()`. If you want to modify the tokenizer loaded from a statistical model, you should modify `nlp.tokenizer` directly.

The prefix, infix and suffix rule sets include not only individual characters but also detailed regular expressions that take the surrounding context into account. For example, there is a regular expression that treats a hyphen between letters as an infix. If you do not want the tokenizer to split on hyphens between letters, you can modify the existing infix definition from [lang/punctuation.py](https://github.com/explosion/spaCy/blob/master/spacy/lang/punctuation.py):

```
import spacy
from spacy.lang.char_classes import ALPHA, ALPHA_LOWER, ALPHA_UPPER
from spacy.lang.char_classes import CONCAT_QUOTES, LIST_ELLIPSES, LIST_ICONS
from spacy.util import compile_infix_regex

# default tokenizer
nlp = spacy.load("en_core_web_sm")
doc = nlp("mother-in-law")
print([t.text for t in doc]) # ['mother', '-', 'in', '-', 'law']

# modify tokenizer infix patterns
infixes = (
    LIST_ELLIPSES
    + LIST_ICONS
    + [
        r"(?<=[0-9])[+\-\*^](?=[0-9-])",
        r"(?<=[{al}{q}])\.(?=[{au}{q}])".format(
            al=ALPHA_LOWER, au=ALPHA_UPPER, q=CONCAT_QUOTES
        ),
        r"(?<=[{a}]),(?=[{a}])".format(a=ALPHA),
        # EDIT: commented out regex that splits on hyphens between letters:
        #r"(?<=[{a}])(?:{h})(?=[{a}])".format(a=ALPHA, h=HYPHENS),
        r"(?<=[{a}0-9])[:<>=/](?=[{a}])".format(a=ALPHA),
    ]
)

infix_re = compile_infix_regex(infixes)
nlp.tokenizer.infix_finditer = infix_re.finditer
doc = nlp("mother-in-law")
print([t.text for t in doc]) # ['mother-in-law']
```

For an overview of the default regular expressions, see [lang/punctuation.py](https://github.com/explosion/spaCy/blob/master/spacy/lang/punctuation.py) and language-specific definitions such as [lang/de/punctuation.py](https://github.com/explosion/spaCy/blob/master/spacy/lang/de/punctuation.py) for German.
